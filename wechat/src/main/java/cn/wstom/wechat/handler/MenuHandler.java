package cn.wstom.wechat.handler;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 按钮事件处理器
 * @author lk
 * @create 2019/9/10
 **/
@Component
public class MenuHandler extends AbstractHandler {
    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
                                    Map<String, Object> map,
                                    WxMpService wxMpService, WxSessionManager wxSessionManager) {
        String msg = String.format("type:%s, event:%s, key:%s",
                wxMpXmlMessage.getMsgType(), wxMpXmlMessage.getEvent(),
                wxMpXmlMessage.getEventKey());
        if (WxConsts.MenuButtonType.VIEW.equals(wxMpXmlMessage.getEvent())) {
            return null;
        }
        //TODO 处理点击事件
        return WxMpXmlOutMessage.TEXT().content(msg)
                .fromUser(wxMpXmlMessage.getToUser()).toUser(wxMpXmlMessage.getFromUser())
                .build();
    }
}
