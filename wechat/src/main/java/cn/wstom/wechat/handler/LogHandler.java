package cn.wstom.wechat.handler;

import com.alibaba.fastjson.JSON;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 统一日志信息返回
 * @author lk
 * @create 2019/9/10
 **/
@Component
public class LogHandler extends AbstractHandler {

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
                                    Map<String, Object> map, WxMpService wxMpService,
                                    WxSessionManager wxSessionManager) {
        this.logger.info("\n【微信服务】接收到请求消息，内容：{}", JSON.toJSONString(wxMpXmlMessage));
        return null;
    }
}
