package cn.wstom.wechat.config;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 微信公众号配置
 * @author lk
 * @create 2019/9/10
 **/
@Data
@ConfigurationProperties(prefix = "wx.mp")
public class WxPropertiesConfig {

    /**
     * 多公众号配置
     */
    private List<MpConfig> configs;

    @Data
    public static class MpConfig {

        /**
         * 微信公众号的 appId
         */
        private String appId;

        /**
         * 微信公众号的 appSecret
         */
        private String secret;

        /**
         * 微信公众号的 token
         */
        private String token;

        /**
         * 微信公众号的 encodingAESKey
         */
        private String aesKey;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
