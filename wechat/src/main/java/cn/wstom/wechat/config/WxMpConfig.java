package cn.wstom.wechat.config;

import cn.wstom.wechat.handler.*;
import lombok.AllArgsConstructor;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

import static me.chanjar.weixin.common.api.WxConsts.EventType.SUBSCRIBE;
import static me.chanjar.weixin.common.api.WxConsts.EventType.UNSUBSCRIBE;
import static me.chanjar.weixin.common.api.WxConsts.MenuButtonType.CLICK;
import static me.chanjar.weixin.common.api.WxConsts.MenuButtonType.VIEW;
import static me.chanjar.weixin.common.api.WxConsts.XmlMsgType.EVENT;

/**
 * 微信公众号 mp（订阅号、服务号） 配置类
 * @author lk
 * @create 2019/9/10
 **/
@AllArgsConstructor
@Configuration
@EnableConfigurationProperties(WxPropertiesConfig.class)
public class WxMpConfig {

    private final WxPropertiesConfig properties;

    private final LogHandler logHandler;

    private final MsgHandler msgHandler;

    private final MenuHandler menuHandler;

    private final DefaultHandler defaultHandler;

    private final SubscribeHandler subscribeHandler;

    private final UnsubscribeHandler unsubscribeHandler;

    private final LocationHandler locationHandler;

    private final ScanHandler scanHandler;

    /**
     * 注入微信服务
     * @return
     */
    @Bean
    public WxMpService wxService() {
        final List<WxPropertiesConfig.MpConfig> configs = this.properties.getConfigs();
        if (configs == null) {
            throw new RuntimeException("【微信服务】找不到微信公众号相关配置！");
        }
        WxMpService service = new WxMpServiceImpl();
        service.setMultiConfigStorages(configs
                .stream().map(e -> {
                    WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
                    configStorage.setAppId(e.getAppId());
                    configStorage.setSecret(e.getSecret());
                    configStorage.setToken(e.getToken());
                    configStorage.setAesKey(e.getAesKey());
                    return configStorage;
                }).collect(Collectors.toMap(WxMpInMemoryConfigStorage::getAppId, a -> a, (o, n) -> o)));
        return service;
    }

    /**
     * 微信消息路由
     * @return
     */
    @Bean
    public WxMpMessageRouter messageRouter(WxMpService wxMpService) {
        final WxMpMessageRouter newRouter = new WxMpMessageRouter(wxMpService);
        // 异步记录日志信息
        newRouter.rule().handler(this.logHandler).next();
        // 消息路由
        newRouter.rule().async(false).handler(this.msgHandler).end();
        // 自定义菜单事件
        newRouter.rule().async(false).msgType(EVENT).event(CLICK).handler(this.menuHandler).end();
        // 链接菜单点击事件
        newRouter.rule().async(false).msgType(EVENT).event(VIEW).handler(this.defaultHandler).end();
        // 关注事件
        newRouter.rule().async(false).msgType(EVENT).event(SUBSCRIBE).handler(this.subscribeHandler).end();
        // 取消关注事件
        newRouter.rule().async(false).msgType(EVENT).event(UNSUBSCRIBE).handler(this.unsubscribeHandler).end();
        // 主动上报地理位置事件
        newRouter.rule().async(false).msgType(EVENT).event(WxConsts.EventType.LOCATION).handler(this.locationHandler).end();
        // 接收地理位置消息
        newRouter.rule().async(false).msgType(WxConsts.XmlMsgType.LOCATION).handler(this.locationHandler).end();
        // 扫码事件
        newRouter.rule().async(false).msgType(EVENT).event(WxConsts.EventType.SCAN).handler(this.scanHandler).end();
        return newRouter;
    }
}
