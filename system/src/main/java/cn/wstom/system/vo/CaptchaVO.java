package cn.wstom.system.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CaptchaVO {


    // ip地址
    private String ip;

    // 过期时间
    private Date endDate;

    // 验证码
    private String captcha;

}
