package cn.wstom.system.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.system.entity.SysDictData;


/**
 * 字典表 数据层
 *
 * @author dws
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

    /**
     * 查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    int countDictDataByType(String dictType);
}
