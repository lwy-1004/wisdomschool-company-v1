package cn.wstom.system.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.system.entity.SysOperLog;

/**
 * 操作日志 服务层
 *
 * @author dws
 */
public interface SysOperLogService extends BaseService<SysOperLog> {

    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
