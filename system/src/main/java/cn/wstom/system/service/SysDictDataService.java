package cn.wstom.system.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.system.entity.SysDictData;

/**
 * 字典 业务层
 *
 * @author dws
 */
public interface SysDictDataService extends BaseService<SysDictData> {
}
