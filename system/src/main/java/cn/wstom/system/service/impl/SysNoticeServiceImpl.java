package cn.wstom.system.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.system.entity.SysNotice;
import cn.wstom.system.mapper.SysNoticeMapper;
import cn.wstom.system.service.SysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 公告 服务层实现
 *
 * @author dws
 * @date 2018-06-25
 */
@Service
public class SysNoticeServiceImpl extends BaseServiceImpl<SysNoticeMapper, SysNotice> implements SysNoticeService {
    @Autowired
    private SysNoticeMapper noticeMapper;

}
