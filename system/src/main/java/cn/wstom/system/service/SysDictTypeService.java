package cn.wstom.system.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.system.entity.SysDictType;

/**
 * 字典 业务层
 *
 * @author dws
 */
public interface SysDictTypeService extends BaseService<SysDictType> {

    /**
     * 校验字典类型称是否唯一
     *
     * @param dictType 字典类型
     * @return 结果
     */
    String checkDictTypeUnique(SysDictType dictType);
}
