-- auto Generated on 2019-01-20 21:16:09 
-- DROP TABLE IF EXISTS `base_entity`; 
CREATE TABLE base_table
(
  `id`          int(11) UNSIGNED                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_by`   int(11) UNSIGNED                                        NOT NULL COMMENT '创建者id',
  `create_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT NULL COMMENT '创建者名称',
  `create_time` DATETIME                                                NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by`   int(11) UNSIGNED                                        NOT NULL COMMENT '最近一次更新者id',
  `update_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT NULL COMMENT '更新者名称',
  `update_time` DATETIME                                                         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark`      varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL COMMENT '备注',
  `version`     INTEGER(12)                                             NOT NULL DEFAULT -1 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Compact COMMENT '基础表';