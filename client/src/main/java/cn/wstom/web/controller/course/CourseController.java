package cn.wstom.web.controller.course;

import cn.wstom.common.base.Data;
import cn.wstom.common.constant.Constants;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.data.TeacherCourseVo;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.service.*;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.onlineexam.entity.TestPaper;
import cn.wstom.onlineexam.entity.TestPaperOne;
import cn.wstom.onlineexam.entity.UserExam;
import cn.wstom.onlineexam.entity.UserTest;
import cn.wstom.onlineexam.service.TestPaperOneService;
import cn.wstom.onlineexam.service.TestPaperService;
import cn.wstom.onlineexam.service.UserExamService;
import cn.wstom.onlineexam.service.UserTestService;
import cn.wstom.square.entity.*;
import cn.wstom.square.service.ForumService;
import cn.wstom.square.service.TopicService;
import cn.wstom.square.service.VideoChapterService;
import cn.wstom.square.service.VideoChapterUserService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/course")
public class CourseController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(CourseController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private TeacherCourseService teacherCourseService;

    @Autowired
    private ChapterResourceService chapterResourceService;
    @Autowired
    private UserExamService userExamService;
    @Autowired
    private ChapterService chapterService;

    @Autowired
    private RecourseTypeService recourseTypeService;

    @Autowired
    private RecourseService recourseService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private ClbumService clbumService;

    @Autowired
    private TestPaperOneService testPaperOneService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private ClbumCourseService clbumCourseService;

    @Autowired
    private TopicService topicService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserTestService userTestService;
    @Autowired
    private VideoChapterService videoChapterService;
    @Autowired
    private VideoChapterUserService videoChapterUserService;
    /**
     *
     * 课程列表获取
     * @param pageNum 页数
     * @return
     */
    @ApiOperation("学生课程列表")
    @GetMapping(value = "/list")
    public Data list(@RequestParam(required = false, defaultValue = "0", value = "pageNum") int pageNum) {
        String studentId = getUser().getUserAttrId();
        Student student = studentService.getById(studentId);
        //  班级
        Clbum clbum = clbumService.getById(student.getCid());
        //  课程数
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("cid", student.getCid());
        int courseCount = clbumCourseService.count(params);
        //  课程列表
        Page page = loadNumData(pageNum);
        List<ClbumCourseVo> courseVoList = courseService.selectCoursesByStuId(Integer.parseInt(studentId));

        Map<String, Object> map = new HashMap<>();
        map.put("clbum", clbum);
        map.put("courseCount", courseCount);
        map.put("courseList", courseVoList);
        map.put("pageSize", page.getPages());
        return Data.success(map);
    }
    /**
     *
     * 课程列表获取
     * @param pageNum 页数
     * @return
     */
    @ApiOperation("教师课程列表")
    @GetMapping(value = "/teacherlist")
    public Data Teacherlist(@RequestParam(required = false, defaultValue = "0", value = "pageNum") int pageNum) {
        String teacherId = getUser().getUserAttrId();
        TeacherCourse teacher = new TeacherCourse();
        teacher.setTid(teacherId);
        //  班级
        ClbumCourse clbum = new ClbumCourse();
        clbum.setTcid(teacher.getId());
        //  课程数
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("cid", clbum.getCid());
        int courseCount = clbumCourseService.count(params);
        //  课程列表
        Page page = loadNumData(pageNum);
//        List<ClbumCourseVo> courseVoList = courseService.selectCoursesByTeaId(teacherId);
        List<TeacherCourse> list = teacherCourseService.list(teacher);
        List<TeacherCourseVo> tcVo = trans(list);
        Map<String, Object> map = new HashMap<>();
        map.put("clbum", clbum);
        map.put("courseCount", courseCount);
        map.put("courseList", tcVo);
        map.put("pageSize", page.getPages());
        return Data.success(map);
    }
    /**
     * 批量转换teacherVo类型
     *
     * @param teacherCourses 将<code>TeacherCourse</code>类型转为<code>TeacherCourseVo</code>
     * @return <code>TeacherCourseVo</code>类型数据
     */
    private List<TeacherCourseVo> trans(List<TeacherCourse> teacherCourses) {
        List<TeacherCourseVo> tcVos = new LinkedList<>();
        Map<String, Course> courseMap = courseService.map(null);

        teacherCourses.forEach(u -> {
            TeacherCourseVo teacherCourseVo = new TeacherCourseVo();
            BeanUtils.copyProperties(u, teacherCourseVo);
            teacherCourseVo.setCourse(courseMap.get(u.getCid()));
            tcVos.add(teacherCourseVo);
        });
        return tcVos;
    }
    /**
     * 课程内容获取
     * @param tcid
     * @return
     */
    @ApiOperation("课程内容")
    @GetMapping("/detail/{tcid}")
    public Data detail(@PathVariable String tcid,
                       @RequestParam(required = false, defaultValue = "0", value = "pageNum") int pageNum,
                       @RequestParam(required = false, defaultValue = "1", value = "sort") int sort) {
        //  课程信息
        TeacherCourse teachCourse = teacherCourseService.getById(tcid);
        Course course = courseService.getById(teachCourse.getCid());

        //  资料类型列表
        RecourseType type = new RecourseType();
        type.setTcId(String.valueOf(tcid));
        List<RecourseType> types = recourseTypeService.list(type);

        //  课程章
        List chapters = chapterService.getCourseChapterInfoTree(teachCourse.getCid());

        //  课程论坛
        List topics = topicService.getCourseAllTopic(teachCourse.getId(), sort);

        //  教师
        SysUser user = sysUserService.selectUserByUserAttrId(teachCourse.getTid());


        Map<String, Object> map = new HashMap<>();
        map.put("course", course);
        if (user != null)
            map.put("teacherName", user.getUserName());
        map.put("types", types);
        map.put("chapters", chapters);
        map.put("topics", topics);
        return Data.success(map);
    }

    /**
     *
     * @param tcid 教授课程
     * @param type 资源类型
     * @param search 搜索字段
     * @return
     */
    @ApiOperation("类型资源内容")
    @PostMapping("/resources/{tcid}")
    public Data resources(

            @RequestParam(required = false, defaultValue = "0", value = "type") String type,
            @RequestParam(required = false, defaultValue = "", value = "search") String search,
            @PathVariable String tcid) {
        RecourseType recourseType = new RecourseType();
        if (!"0".equals(type)) {
            recourseType.setId(type);
        }

        Recourse recourse = new Recourse();
        recourse.setCategory(recourseType);
        if (StringUtils.isNotEmpty(search)) {
            recourse.setName(search);
        }
        recourse.setTcId(tcid);
        //  加载指定num 数据

        List<Recourse> result = recourseService.list(recourse);
        for (int i = 0; i < result.size(); i++) {
            
        }
        if (!result.isEmpty()){
            ChapterResource chapterResource = new ChapterResource();
            chapterResource.setTcId(result.get(0).getTcId());
            chapterResource.setResourceType(result.get(0).getRecourseType());
            List<ChapterResource> chapterResourceList = chapterResourceService.selectAttrId(chapterResource);
            System.out.println("chapterResourceList:"+chapterResourceList);
            return Data.success(chapterResourceList);
        }
        return Data.success(result);
    }

    @ApiOperation("类型资源内容")
    @PostMapping("/resources1")
    public Data resources1() {
        String studentId = getUser().getUserAttrId();
        List<Recourse> result = recourseService.getResources(studentId);
        return Data.success(result);
    }
    @ApiOperation("ppt资源内容")
    @PostMapping("/resourcesppt")
    public Data  getResourcePpt() {
        String studentId = getUser().getUserAttrId();
        List<Recourse> result = recourseService.getResourcePpt(studentId);
        return Data.success(result);
    }
    @ApiOperation("视频资源内容")
    @PostMapping("/resourcesvideo")
    public Data  getResourceVideo() {
        String studentId = getUser().getUserAttrId();
        List<Recourse> result = recourseService.getResourceVideo(studentId);
        return Data.success(result);
    }
    @ApiOperation("考试试卷")
    @PostMapping("/exam/{cid}/{tcid}/{resourceType}")
    public Data chapterResources(
            @PathVariable String cid,
            @PathVariable String tcid,
            @PathVariable Integer resourceType) {
      ChapterResource chapterResource = new ChapterResource();
      chapterResource.setCid(cid);
      chapterResource.setTcId(tcid);
      chapterResource.setResourceType(resourceType);
        List<ChapterResource> result = chapterResourceService.list(chapterResource);
        System.out.println(result);
        return Data.success(result);
    }
    @ApiOperation("学生观看详情")
    @PostMapping("/statis/showVideoDetail/{tcid}/{userId}")
    public Data videoList(@PathVariable("tcid") String tcId,@PathVariable("userId") String userId, VideoChapterUserVo vo)throws  Exception{
        SysUser stu = null;

        try {
            stu = sysUserService.getById(userId);
        } catch (Exception e) {
            throw new Exception("未找到该学生用户");
        }

        VideoChapter vc = new VideoChapter();
        vc.setCourseTeacherId(Integer.valueOf(tcId));
        startPage();
        List<VideoChapter> vcs = videoChapterService.list(vc);

        if (!CollectionUtils.isEmpty(vcs)) {
            SysUser finalStu = stu;
            vcs.forEach(v -> {
                ChapterResource cr = chapterResourceService.getById(String.valueOf(v.getResourceChapterId()));
                Chapter chapter = null;
                Recourse recourse = null;
                if (cr != null) {
                    chapter = chapterService.getById(cr.getCid());
                    recourse = recourseService.getById(cr.getRid());
                }

                VideoChapterUser vcu = new VideoChapterUser();
                vcu.setVideoChapterId(Integer.valueOf(v.getId()));
                vcu.setUserId(Integer.valueOf(userId));

                Map<String, Object> params = new HashMap<>();
                List vcus = videoChapterUserService.list(vcu);
                if (!vcus.isEmpty()) {
                    params.put("videoChapterUser", vcus.get(0));
                } else {
                    params.put("videoChapterUser", null);
                }
                if (chapter != null)
                    params.put("chapterName", chapter.getName());
                else params.put("chapterName", "未找到章节");
                if (recourse != null) {
                    params.put("resourceName", recourse.getName());
                } else params.put("resourceName", "未找到资源");

                if (finalStu != null)
                    params.put("studentName", finalStu.getUserName());
                v.setParams(params);
            });
        }
        return Data.success(vcs);
    }
    @ApiOperation("学生测试详情")
    @PostMapping("/statis/showTestDetail/{userId}/{tcid}")
    public Data courseStatis(@PathVariable("userId") String userId,
                             @PathVariable("tcid") String tcId ){
        SysUser stu = sysUserService.getById(userId);
        TeacherCourse tc = teacherCourseService.getById(tcId);

        TestPaper paper = new TestPaper();
        paper.setCoursrId(tc.getCid());
        paper.setCreateId(tc.getTid());
        paper.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
        List<TestPaper> testPapers = testPaperService.list(paper);
        testPapers.forEach(p -> {
            Map<String, Object> params = new HashMap<>();
            params.put("stuNum", stu.getLoginName());
            params.put("stuName", stu.getUserName());
            params.put("userId", stu.getId());

            UserTest ut = new UserTest();
            ut.setcId(tc.getCid());
            ut.setUserId(userId);
            ut.setTestPaperId(p.getId());
            List uts = userTestService.selectListBase(ut);
            if (!uts.isEmpty()) {
                params.put("userTest", uts.get(0));
            } else {
                params.put("userTest", null);
            }

            p.setParams(params);
        });
        return Data.success(testPapers);
    }
    @ApiOperation("学生考试详情")
    @PostMapping("/statis/showExamDetail/{userId}/{tcid}")
    public Data courseExamStatis(@PathVariable("userId") String userId,
                             @PathVariable("tcid") String tcId ){
        SysUser stu = sysUserService.getById(userId);
        TeacherCourse tc = teacherCourseService.getById(tcId);

        TestPaperOne paper = new TestPaperOne();
        paper.setCoursrId(tc.getCid());
        paper.setCreateId(tc.getTid());
        paper.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
        List<TestPaperOne> testPapers = testPaperOneService.list(paper);

        testPapers.forEach(p -> {
            Map<String, Object> params = new HashMap<>();
            params.put("stuNum", stu.getLoginName());
            params.put("stuName", stu.getUserName());
            params.put("userId", stu.getId());

            UserExam ut = new UserExam();
            ut.setcId(tc.getCid());
            ut.setUserId(userId);
            ut.setTestPaperOneId(p.getId());
            List uts = userExamService.selectListBase(ut);
            if (!uts.isEmpty()) {
                params.put("userExam", uts.get(0));
            } else {
                params.put("userExam", null);
            }

            p.setParams(params);
        });
        return Data.success(testPapers);
    }
    @ApiOperation("学习统计")
    @PostMapping("/statis/{cid}/{tid}")
    public Data courseStatis(@PathVariable("cid") String cid,
                             @PathVariable("tid") String tid,
                             SysUser user,
                             Model model) {
        TeacherCourse tcI = new TeacherCourse();
        tcI.setCid(cid);
        tcI.setTid(tid);
        TeacherCourse tc = teacherCourseService.list(tcI).get(0);
        model.addAttribute("tid", tid);
        model.addAttribute("tcId", tc.getId());

        String clbumId = user.getNation();
        String loginName = user.getLoginName();
        if (user.getLoginName() == null || user.getLoginName().equals("")) {
            loginName = null;
        }
        if (user.getNation() == null || user.getNation().equals("")) {
            clbumId = null;
        }
        //  课堂班级学生列表
        startPage();
        List<SysUser> users = userService.selectStudentByCourseIdAndTeacherId(cid, tid, loginName, clbumId);

        user.setNation(null);
        users.forEach(u -> {
            u.setPassword("");
            Student student = studentService.getById(u.getUserAttrId());
            Clbum clbum = clbumService.getById(student.getCid());

            AtomicReference<Integer> watchVideoCount = new AtomicReference<>(0);
            VideoChapter vc = new VideoChapter();
            vc.setCourseTeacherId(Integer.valueOf(tc.getId()));
            List<VideoChapter> vcs = videoChapterService.list(vc);
            VideoChapterUser videoUser = new VideoChapterUser();
            videoUser.setUserId(Integer.valueOf(u.getId()));
            vcs.forEach(v -> {
                videoUser.setVideoChapterId(Integer.valueOf(v.getId()));
                if (!videoChapterUserService.list(videoUser).isEmpty()) {
                    watchVideoCount.getAndSet(watchVideoCount.get() + 1);
                }
            });

            //TestPaper Count
            UserTest param = new UserTest();
            param.setcId(cid);
            param.setUserId(u.getId());
            param.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
            List userTests = userTestService.selectListBase(param);
            UserExam param1 = new UserExam();
            param1.setcId(cid);
            param1.setUserId(u.getId());
            param1.setType(Constants.TEST_PAPER_TYPE_CHAPTER);
            List userExams = userExamService.selectListBase(param1);
            Map<String, Object> object = new HashMap<>();
            object.put("clbumName", clbum.getName());
            object.put("questionCount", userTests.size());
            object.put("questionCount1", userExams.size());
            object.put("watchVideoCount", watchVideoCount);
            object.put("tcid",tc.getId());
            u.setParams(object);
        });
        return Data.success(users);
    }

    /**
     * 视频播放
     * MultiValueMap<String, Object> params = new LinkedMultiValueMap<>(1);
     *         params.add("fileId", fid);
     *         Data result = restOperations.postForObject(STORAGE_URL_PREFIX + "/playVideo", params, Data.class);
     */
}
