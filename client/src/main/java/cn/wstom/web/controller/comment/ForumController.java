package cn.wstom.web.controller.comment;

import cn.wstom.common.base.Data;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.square.entity.Forum;
import cn.wstom.square.entity.Topic;
import cn.wstom.square.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.core.http.ResultContracts;
import cn.wstom.web.core.http.ResultInfo;
import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/forum")
public class ForumController extends BaseController {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ForumService forumService;
    @Autowired
    private TopicService topicService;
    @Autowired
    private ReplyService replyService;
    @Autowired
    private DeckService deckService;
    @Autowired
    private ArticleService articleService;

    @ApiOperation("板块列表")
    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public ResultInfo forumList() {
        List<Forum> forums = forumService.list(new Forum());
        Map<String, Object> map = new HashMap<>();
        map.put("forums", forums);
        return new ResultInfo(ResultContracts.SUCCESS, map);
    }

    @ApiOperation("板块内容")
    @RequestMapping(method = RequestMethod.GET, value = "/{forumId}")
    public ResultInfo topic(@RequestParam(required = false, defaultValue = "0", value = "pageNum") int pageNum,
                      @PathVariable(value = "forumId") String forumId) {
        Forum forum = new Forum();
        forum.setId(forumId);
        Topic topic = new Topic();
        topic.setForum(forum);
        Page page = loadNumData(pageNum);
        List<Topic> topics = topicService.list(topic);

        topics.forEach(t -> {
            SysUser user = sysUserService.getById(t.getCreateBy());
            if (user != null && StringUtils.isNotEmpty(user.getAvatar())) {
                t.setUserAvatar(user.getAvatar());
            }
        });


        Map<String, Object> data = new HashMap<>();
        data.put("userId", getUserId());
        data.put("topics", topics);
        data.put("pageSize", page.getPages());
        return new ResultInfo(ResultContracts.SUCCESS, data);
    }

}
