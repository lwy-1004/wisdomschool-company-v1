package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Outpeizhi;

/**
 * @author zzr
 * @date 2019/03/28
 */
public interface OutpeizhiService extends BaseService<Outpeizhi> {
}
