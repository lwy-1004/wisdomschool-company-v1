package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Outpeizhi;
import cn.wstom.jiaowu.mapper.OutpeizhiMapper;
import cn.wstom.jiaowu.service.OutpeizhiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zzr
 * @date 2019/03/28
 */
@Service
public class OutpeizhiServiceImpl extends BaseServiceImpl<OutpeizhiMapper, Outpeizhi> implements OutpeizhiService {

    @Autowired
    private OutpeizhiMapper outpeizhiMapper;
}
