package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Lead;

/**
 * @author dws
 * @date 2019/03/28
 */
public interface LeadService extends BaseService<Lead> {
}
