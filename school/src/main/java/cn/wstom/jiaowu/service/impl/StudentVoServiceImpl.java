package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.data.StudentVo;
import cn.wstom.jiaowu.mapper.StudentVoMapper;
import cn.wstom.jiaowu.service.StudentVoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentVoServiceImpl extends BaseServiceImpl<StudentVoMapper, StudentVo> implements StudentVoService {

    @Resource
    private StudentVoMapper studentVoMapper;

    @Override
    public List<StudentVo> selectByStudentVos(StudentVo studentVo) {
        return studentVoMapper.selectByStudentVos(studentVo);
    }
}
