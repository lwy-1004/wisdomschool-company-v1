package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.ClbumCourse;
import cn.wstom.jiaowu.mapper.ClbumCourseMapper;
import cn.wstom.jiaowu.service.ClbumCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 班级课程 服务层实现
 *
 * @author hyb
 * @date 20190218
 */
@Service
public class ClbumCourseServiceImpl extends BaseServiceImpl
        <ClbumCourseMapper, ClbumCourse>
        implements ClbumCourseService {

    @Autowired
    private ClbumCourseMapper clbumCourseMapper;
}
