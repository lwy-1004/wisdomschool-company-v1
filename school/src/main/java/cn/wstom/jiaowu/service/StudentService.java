package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.Student;
import cn.wstom.square.entity.Statistics;

/**
 * 学生service
 *
 * @author dws
 * @date 2019/01/05
 */
public interface StudentService extends BaseService<Student> {

    /**
     * 统计指定的学生数据
     *
     * @param userId
     * @return
     */
    Statistics statisticsById(String userId);
}
