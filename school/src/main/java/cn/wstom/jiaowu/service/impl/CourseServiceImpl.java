package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.common.constant.ForumConstants;
import cn.wstom.common.entity.Comment;
import cn.wstom.common.mapper.CommentMapper;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.mapper.ClbumCourseVoMapper;
import cn.wstom.jiaowu.mapper.CourseMapper;
import cn.wstom.jiaowu.service.CourseService;
import cn.wstom.jiaowu.service.StudentService;
import cn.wstom.jiaowu.service.TeacherService;
import cn.wstom.square.data.PageVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程 服务层实现
 *
 * @author hyb
 * @date 2019-01-29
 */
@Transactional
@Service
public class CourseServiceImpl extends BaseServiceImpl<CourseMapper, Course>
        implements CourseService {

    @Resource
    private CourseMapper courseMapper;

    @Resource
    private CommentMapper commentMapper;

    @Resource
    private StudentService studentService;
    @Resource
    private TeacherService teacherService;
    @Resource
    private ClbumCourseVoMapper clbumCourseVoMapper;


    @Override
    public Map<String, Course> mapByClbumId(List<String> cid) {
        List<Course> courses = courseMapper.selectBatchClbumIds(cid);
        Map<String, Course> result = new LinkedHashMap<>();
        courses.forEach(course -> result.put(course.getId(), course));
        return result;
    }

    @Override
    public TeacherCourse getTeacherCourse(String userAttrId, String courseId) {
        return courseMapper.selectTeacherCourse(userAttrId, courseId);
    }

    @Override
    public PageVo<Comment> getCourseCommentListPage(String courseId, String userId, String createTime, String parentId, String orderby, String order, int pageNum, int rows) {
        PageVo<Comment> pageVo = new PageVo<>(pageNum);
        pageVo.setRows(rows);
        if (orderby == null) {
            orderby = "id";
        }
        if (order == null) {
            order = "desc";
        }
        pageVo.setList(commentMapper.selectCommentList(ForumConstants.INFO_TYPE_COURSE, courseId, userId, createTime, parentId, orderby, order, pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(commentMapper.selectCommentCount(ForumConstants.INFO_TYPE_COURSE, courseId, userId, createTime, parentId));
        return pageVo;
    }

    @Override
    public List<ClbumCourseVo> selectCoursesByStuId(int studentId) {
        Student student = studentService.getById(studentId);
        return clbumCourseVoMapper.selectClbumCourseVosByClbumId(student.getCid());
    }

    @Override
    public List<ClbumCourseVo> selectCoursesByTeaId(String teacherId) {
        TeacherCourse teacher = new TeacherCourse();
        teacher.setTid(teacherId);
        ClbumCourse clbum = new ClbumCourse();
        clbum.setTcid(teacher.getId());
        return clbumCourseVoMapper.selectClbumCourseVosByClbumId(clbum.getCid());
    }

    @Override
    public List<Course> selectByCourses(Course course) {
        return courseMapper.selectByCourses(course);
    }

}
