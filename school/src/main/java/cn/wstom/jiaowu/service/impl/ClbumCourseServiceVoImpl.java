package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.entity.ClbumCourse;
import cn.wstom.jiaowu.mapper.ClbumCourseMapper;
import cn.wstom.jiaowu.mapper.ClbumCourseVoMapper;
import cn.wstom.jiaowu.service.ClbumCourseService;
import cn.wstom.jiaowu.service.ClbumCourseVoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 班级课程 服务层实现
 *
 * @author hyb
 * @date 20190218
 */
@Service
public class ClbumCourseServiceVoImpl extends BaseServiceImpl
        <ClbumCourseVoMapper<ClbumCourse>, ClbumCourse>
        implements ClbumCourseVoService<ClbumCourse> {
    /*  Lin_   ClbumCourseServiceVoImpl */
    @Autowired
    private ClbumCourseVoMapper clbumCourseVoMapper;

    @Override
    public List<ClbumCourse> selectByClbumCourseVos(ClbumCourse clbumCourse) {
        return clbumCourseVoMapper.selectByClbumCourseVos(clbumCourse);
    }
}
