package cn.wstom.jiaowu.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Grades;
import cn.wstom.jiaowu.mapper.GradesMapper;
import cn.wstom.jiaowu.service.GradesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 年级 服务层实现
 *
 * @author xyl
 * @date 2019-01-22
 */
@Service
public class GradesServiceImpl extends BaseServiceImpl<GradesMapper, Grades>
        implements GradesService {

    @Autowired
    private GradesMapper gradesMapper;
}
