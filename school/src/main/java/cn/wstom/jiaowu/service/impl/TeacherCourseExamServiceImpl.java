package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.TeacherCourse;
import cn.wstom.jiaowu.entity.TeacherCourseExam;
import cn.wstom.jiaowu.mapper.TeacherCourseExamMapper;
import cn.wstom.jiaowu.mapper.TeacherCourseMapper;
import cn.wstom.jiaowu.service.TeacherCourseExamService;
import cn.wstom.jiaowu.service.TeacherCourseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 教师课程 服务层实现
 *
 * @author hyb
 * @date 2019-01-29
 */
@Service
public class TeacherCourseExamServiceImpl extends BaseServiceImpl
        <TeacherCourseExamMapper, TeacherCourseExam>
        implements TeacherCourseExamService {

    @Resource
    private TeacherCourseExamMapper teacherCourseExamMapper;
    @Override
    public TeacherCourseExam selectCourseId(String courseId) {
        return teacherCourseExamMapper.selectCourseId(courseId);
    }
}
