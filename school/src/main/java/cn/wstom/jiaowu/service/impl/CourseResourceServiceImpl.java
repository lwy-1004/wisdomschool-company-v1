package cn.wstom.jiaowu.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.ChapterResource;
import cn.wstom.jiaowu.entity.CourseResource;
import cn.wstom.jiaowu.mapper.ChapterResourceMapper;
import cn.wstom.jiaowu.mapper.CourseResourceMapper;
import cn.wstom.jiaowu.service.ChapterResourceService;
import cn.wstom.jiaowu.service.CourseResourceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 课程章节资源 服务层实现
 *
 * @author dws
 * @date 20190223
 */
@Service
public class CourseResourceServiceImpl extends BaseServiceImpl<CourseResourceMapper, CourseResource>
        implements CourseResourceService {

    @Resource
    private CourseResourceMapper courseResourceMapper;

    @Override
    public List<CourseResource> selectByCidAndSid(String userId, String courseId) {
        return courseResourceMapper.selectByCidAndSid(userId, courseId);
    }

    @Override
    public int saveBackId(CourseResource courseResource) {
        return courseResourceMapper.insertBackId(courseResource);
    }
}
