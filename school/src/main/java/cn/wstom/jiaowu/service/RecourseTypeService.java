package cn.wstom.jiaowu.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.RecourseType;
import cn.wstom.onlineexam.entity.MyTitleType;

import java.util.List;

/**
 * 资源 服务层
 *
 * @author dws
 * @date 20190222
 */
public interface RecourseTypeService extends BaseService<RecourseType> {
    public List<RecourseType> findByTCid(String tcid);
}
