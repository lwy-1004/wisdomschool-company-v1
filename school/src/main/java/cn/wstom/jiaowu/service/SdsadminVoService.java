package cn.wstom.jiaowu.service;
import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.data.SdsadminVo;
import cn.wstom.jiaowu.data.StudentVo;

import java.util.List;

public interface SdsadminVoService extends BaseService<SdsadminVo> {
    List<SdsadminVo> selectBySdsadminVos(SdsadminVo sdsadminVo);
    List<SdsadminVo> selectBySdsadminVo2(SdsadminVo sdsadminVo);
}