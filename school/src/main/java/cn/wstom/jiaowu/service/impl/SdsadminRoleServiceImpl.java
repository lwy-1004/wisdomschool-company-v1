package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.SdsadminRole;
import cn.wstom.jiaowu.mapper.SdsadminRoleMapper;
import cn.wstom.jiaowu.service.SdsadminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SdsadminRoleServiceImpl extends BaseServiceImpl<SdsadminRoleMapper, SdsadminRole>
        implements SdsadminRoleService {
    @Autowired
    private SdsadminRoleMapper sdsadminRoleMapper;
    @Override
    public SdsadminRole selectBySid(Integer sid) {
        return sdsadminRoleMapper.selectBySid(sid);
    }

    @Override
    public int deleteBySid(Integer sid) {
        return sdsadminRoleMapper.deleteBySid(sid);
    }
}
