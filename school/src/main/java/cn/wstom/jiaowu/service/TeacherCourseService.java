package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.TeacherCourse;

/**
 * 教师课程 服务层
 *
 * @author hyb
 * @date 2019-01-29
 */
public interface TeacherCourseService extends BaseService<TeacherCourse> {
    String selectCourseId(String courseId);
    TeacherCourse selectId(String cid,String tid);

}
