package cn.wstom.jiaowu.service.impl;
import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.Sdsadmin;

import cn.wstom.jiaowu.mapper.SdsadminMapper;

import cn.wstom.jiaowu.service.SdsadminService;
import org.springframework.stereotype.Service;

@Service
public class SdsadminServiceImpl extends BaseServiceImpl<SdsadminMapper, Sdsadmin>
        implements SdsadminService {

}