package cn.wstom.jiaowu.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.jiaowu.entity.ClbumCourse;

/**
 * 班级课程 服务层
 *
 * @author hyb
 * @date 20190218
 */
public interface ClbumCourseService extends BaseService<ClbumCourse> {
}
