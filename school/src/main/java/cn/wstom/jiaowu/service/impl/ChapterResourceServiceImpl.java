package cn.wstom.jiaowu.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.ChapterResource;
import cn.wstom.jiaowu.mapper.ChapterResourceMapper;
import cn.wstom.jiaowu.service.ChapterResourceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 课程章节资源 服务层实现
 *
 * @author dws
 * @date 20190223
 */
@Service
public class ChapterResourceServiceImpl extends BaseServiceImpl<ChapterResourceMapper, ChapterResource>
        implements ChapterResourceService {

    @Resource
    private ChapterResourceMapper chapterResourceMapper;
    @Override
    public boolean updateState(ChapterResource chapterResource) {
        return chapterResourceMapper.updateState(chapterResource);
    }
    @Override
    public List<ChapterResource> selectByCidAndSid(String userId, String courseId, String chapterId) {
        return chapterResourceMapper.selectByCidAndSid(userId, courseId, chapterId);
    }

    @Override
    public int saveBackId(ChapterResource chapterResource) {
        return chapterResourceMapper.insertBackId(chapterResource);
    }

    @Override
    public List<ChapterResource> selectTestPaperOneId(String id) {
        return chapterResourceMapper.selectTestPaperOneId(id);
    }

    @Override
    public List<ChapterResource> selectAttrId(ChapterResource chapterResource) {
        return chapterResourceMapper.selectAttrId(chapterResource);
    }

    @Override
    public int updatePptState(String name,String userId) {
        return chapterResourceMapper.updatePptState(name,userId);
    }
}
