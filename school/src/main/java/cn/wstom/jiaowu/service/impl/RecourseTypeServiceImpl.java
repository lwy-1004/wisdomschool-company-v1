package cn.wstom.jiaowu.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.RecourseType;
import cn.wstom.jiaowu.mapper.RecourseTypeMapper;
import cn.wstom.jiaowu.service.RecourseTypeService;
import cn.wstom.onlineexam.entity.MyTitleType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 资源 服务层实现
 *
 * @author dws
 * @date 2019/02/22
 */
@Service
public class RecourseTypeServiceImpl extends BaseServiceImpl<RecourseTypeMapper, RecourseType>
        implements RecourseTypeService {

    @Resource
    private RecourseTypeMapper recourseTypeMapper;
    public List<RecourseType> findByTCid(String tcid){
        return recourseTypeMapper.selectByTCid(tcid);
    }
}
