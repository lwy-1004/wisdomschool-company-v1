package cn.wstom.jiaowu.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.jiaowu.entity.TeacherCourse;
import cn.wstom.jiaowu.mapper.TeacherCourseMapper;
import cn.wstom.jiaowu.service.TeacherCourseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 教师课程 服务层实现
 *
 * @author hyb
 * @date 2019-01-29
 */
@Service
public class TeacherCourseServiceImpl extends BaseServiceImpl
        <TeacherCourseMapper, TeacherCourse>
        implements TeacherCourseService {

    @Resource
    private TeacherCourseMapper teacherCourseMapper;
    @Override
    public String selectCourseId(String courseId) {
        return teacherCourseMapper.selectCourseId(courseId);
    }

    @Override
    public TeacherCourse selectId(String cid, String tid) {
        return teacherCourseMapper.selectId(cid,tid);
    }


}
