package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Clbum;

/**
 * 班级 数据层
 *
 * @author dws
 * @date 2019-01-25
 */
public interface ClbumMapper extends BaseMapper<Clbum> {

}