package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.data.StudentVo;

import java.util.List;

public interface StudentVoMapper extends BaseMapper<StudentVo> {
    List<StudentVo> selectByStudentVos(StudentVo studentVo);
}
