package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.entity.ClbumCourse;

import java.util.List;

/**
 * 班级课程 数据层
 *
 * @author hyb
 * @date 20190218
 */
public interface ClbumCourseVoMapper<T extends ClbumCourse> extends BaseMapper<T> {

    /*  Lin_ ClbumCourseMapper */
    List<T> selectByClbumCourseVos(T clbumCourse);

    List<T> selectClbumCourseVosByClbumId(String clbumId);
}