package cn.wstom.jiaowu.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Student;
import cn.wstom.square.entity.Statistics;

/**
 * 学生mapper
 * @author dws
 * @date 2019/01/05
 */
public interface StudentMapper extends BaseMapper<Student> {
    /**
     * 查找指定学生的统计信息
     *
     * @param userId
     * @return
     */
    Statistics statisticsById(String userId);
}
