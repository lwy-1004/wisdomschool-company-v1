package cn.wstom.jiaowu.mapper;
import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.data.SdsadminVo;
import cn.wstom.jiaowu.data.StudentVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface SdsadminVoMapper extends BaseMapper<SdsadminVo> {
    List<SdsadminVo> selectBySdsadminVo(SdsadminVo sdsadminVo);
    List<SdsadminVo> selectBySdsadminVo2(SdsadminVo sdsadminVo);
}
