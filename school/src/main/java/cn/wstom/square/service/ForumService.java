package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.Forum;
import cn.wstom.square.entity.Topic;

import java.util.List;

/**
 * @author dws
 * @date 2019/03/31
 */
public interface ForumService extends BaseService<Forum> {
}
