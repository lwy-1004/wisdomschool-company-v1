package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.data.PageVo;
import cn.wstom.square.entity.Favorite;
import org.springframework.transaction.annotation.Transactional;

/**
 * 收藏记录
 *
 * @author dws
 */
public interface FavoriteService extends BaseService<Favorite> {
    @Transactional(rollbackFor = Exception.class)
    boolean addFavorite(String userId, Integer infoType, String infoId);

    boolean checkFavoriteByUser(String userId, Integer infoType, String infoId);

    PageVo<Favorite> getFavoriteListPage(String userId, Integer infoType, String createTime, String orderby, String order, int pageNum, int rows);
}
