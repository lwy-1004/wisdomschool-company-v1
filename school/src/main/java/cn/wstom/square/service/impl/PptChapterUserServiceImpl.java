package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.PptChapterUser;
import cn.wstom.square.entity.VideoChapter;
import cn.wstom.square.entity.VideoChapterUser;
import cn.wstom.square.mapper.PptChapterUserMapper;
import cn.wstom.square.mapper.VideoChapterMapper;
import cn.wstom.square.mapper.VideoChapterUserMapper;
import cn.wstom.square.service.PptChapterUserService;
import cn.wstom.square.service.VideoChapterService;
import cn.wstom.square.service.VideoChapterUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PptChapterUserServiceImpl  extends BaseServiceImpl<PptChapterUserMapper, PptChapterUser>
        implements PptChapterUserService {
    @Autowired
    PptChapterUserMapper pptChapterUserMapper;

    @Override
    public PptChapterUser getBySidRid(PptChapterUser pptChapterUser) {
        return pptChapterUserMapper.getBySidRid(pptChapterUser);
    }

    @Override
    public List<PptChapterUser> listbycrids(List<String> crids) {
        return pptChapterUserMapper.listbycrids(crids);
    }

    @Override
    public List<PptChapterUser> listbycridsAndsid(PptChapterUser pptChapterUser) {
        return pptChapterUserMapper.listbycridsAndsid(pptChapterUser);
    }
}
