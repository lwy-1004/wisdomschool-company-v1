/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.Channel;

/**
 * 栏目管理
 *
 * @author dws
 */
public interface ChannelService extends BaseService<Channel> {
}
