package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.Discuss;

/**
 * @author dws
 * @date 2019/03/31
 */
public interface DiscussService extends BaseService<Discuss> {
}
