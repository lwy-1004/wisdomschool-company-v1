package cn.wstom.square.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.square.entity.TopicComment;

/**
 * @author liniec
 * @date 2020/01/26 12:24
 */
public interface TopicCommentService extends BaseService<TopicComment> {
}
