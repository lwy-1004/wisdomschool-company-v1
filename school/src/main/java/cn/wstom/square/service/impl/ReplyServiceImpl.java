/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 */

package cn.wstom.square.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.square.entity.Reply;
import cn.wstom.square.mapper.ReplyMapper;
import cn.wstom.square.service.ReplyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author dws
 */
@Service
public class ReplyServiceImpl extends BaseServiceImpl
        <ReplyMapper, Reply>
        implements ReplyService {

    @Resource
    private ReplyMapper replyMapper;

}
