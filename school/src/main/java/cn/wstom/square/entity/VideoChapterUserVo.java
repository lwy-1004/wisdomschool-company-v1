package cn.wstom.square.entity;

import cn.wstom.common.base.entity.BaseEntity;
import lombok.Data;

@Data
public class VideoChapterUserVo extends BaseEntity {
    private VideoChapter videoChapter;

    private VideoChapterUser videoChapterUser;

    @Override
    public String toString() {
        return videoChapter.toString() + videoChapterUser.toString();
    }
}