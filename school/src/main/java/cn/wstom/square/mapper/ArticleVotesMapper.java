package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.ArticleVotes;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface ArticleVotesMapper extends BaseMapper<ArticleVotes> {
}
