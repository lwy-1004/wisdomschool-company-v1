package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Channel;

/**
 * 栏目 数据层
 *
 * @author dws
 * @date 20190227
 */
public interface ChannelMapper extends BaseMapper<Channel> {

}