package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.system.entity.Notify;

/**
 * 消息 数据层
 *
 * @author dws
 * @date 20190227
 */
public interface NotifyMapper extends BaseMapper<Notify> {


}