package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.PptChapterUser;
import cn.wstom.square.entity.VideoChapterUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PptChapterUserMapper extends BaseMapper<PptChapterUser> {
    PptChapterUser getBySidRid(PptChapterUser pptChapterUser);
    List<PptChapterUser> listbycrids(List<String> crids);
    List<PptChapterUser> listbycridsAndsid(PptChapterUser pptChapterUser);
}
