package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.QuestionCount;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface QuestionCountMapper extends BaseMapper<QuestionCount> {
    QuestionCount findQuestionCountById(String questionId);
}
