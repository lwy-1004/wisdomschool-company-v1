package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Reply;

/**
 *
 */
public interface ReplyMapper extends BaseMapper<Reply> {
}
