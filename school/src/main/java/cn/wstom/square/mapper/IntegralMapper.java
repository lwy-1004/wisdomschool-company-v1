package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.jiaowu.entity.Integral;

/**
 * @author dws
 */
public interface IntegralMapper extends BaseMapper<Integral> {
}
