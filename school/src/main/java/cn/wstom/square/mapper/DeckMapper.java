package cn.wstom.square.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.square.entity.Deck;

/**
 *
 */
public interface DeckMapper extends BaseMapper<Deck> {
}
