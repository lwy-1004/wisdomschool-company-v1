package cn.wstom.common.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.common.entity.Banner;

/**
* 轮播图片 服务层
*
* @author dws
* @date 20200131
*/
public interface BannerService extends BaseService<Banner> {
}
