package cn.wstom.common.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.common.entity.Comment;

import java.util.List;

/**
 * @author dws
 * @date 2019/03/07
 */
public interface CommentService extends BaseService<Comment> {
    List<Comment> listByPids(List<String> childrenIds);
}
