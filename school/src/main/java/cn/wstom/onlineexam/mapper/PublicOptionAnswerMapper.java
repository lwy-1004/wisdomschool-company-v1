package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.PublicOptionAnswer;


public interface PublicOptionAnswerMapper extends BaseMapper<PublicOptionAnswer> {


}