package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestPaperOne;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 试卷 数据层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestPaperOneMapper extends BaseMapper<TestPaperOne> {
    /**
     * 校验标题名唯一
     *
     * @param testPaper
     * @return
     */
    TestPaperOne checkTestNameUnique(String testPaper);

    /**
     * 查找年级
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getStudentInfo(@Param("teacherId") String teacherId);

    /**
     * 获取科目
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getTcoName(@Param("teacherId") String teacherId);

    /**
     * 获取学生
     *
     * @param teacherId
     * @return
     */
    List<TestPaperOne> getTcoStu(@Param("teacherId") String teacherId);


    /**
     * 查找学生试卷
     * @param testPaper
     * @return
     */
    List<TestPaperOne> findStuPaper(TestPaperOne testPaper);

    boolean updateSetExam(TestPaperOne testPaper);

}
