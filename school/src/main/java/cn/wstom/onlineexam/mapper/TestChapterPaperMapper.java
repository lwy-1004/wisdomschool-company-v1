package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestChapterPaper;

/**
* 节的试卷 数据层
*
* @author dws
* @date 20190314
*/
public interface TestChapterPaperMapper extends BaseMapper<TestChapterPaper> {

}