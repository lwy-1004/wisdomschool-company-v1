package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.MyQuestions;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @date: 2019-01-18 下午 8:56
 */

public interface MyQuestionsMapper extends BaseMapper<MyQuestions> {
    /**
     * 按条件查询问题
     */
    List<MyQuestions> selectList(MyQuestions myQuestions);

    /**
     * 查询未整理的代码
     * @param myQuestions
     * @return
     */
    List<MyQuestions> unList(MyQuestions myQuestions);

    /**
     * 由Id查询未整理的代码
     * @param id
     * @return
     */
    MyQuestions getUnListById(String id);

    /**
     * 查询一门科目中的一种题型的全部试题
     *
     * @param titleTypeId
     * @param xzsubjectsId
     * @return
     */
    List<MyQuestions> selectQuestionByTid(@Param("titleTypeId") String titleTypeId, @Param("xzsubjectsId") String xzsubjectsId);

}