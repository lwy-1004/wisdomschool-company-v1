package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.Websocket;

/**
* 存放考试的临时 数据层
*
* @author dws
* @date 20190308
*/
public interface WebsocketMapper extends BaseMapper<Websocket> {

}