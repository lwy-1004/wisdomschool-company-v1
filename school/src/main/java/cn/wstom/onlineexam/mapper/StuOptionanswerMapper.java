package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.StuOptionanswer;

/**
 * 学生考试答案 数据层
 *
 * @author hzh
 * @date 20190304
 */
public interface StuOptionanswerMapper extends BaseMapper<StuOptionanswer> {

    int updateByIdAns(StuOptionanswer stuOptionanswer);

}