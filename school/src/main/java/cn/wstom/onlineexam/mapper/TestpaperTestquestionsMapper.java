package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestpaperTestquestions;

/**
 * 试卷做的题目答案 数据层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestpaperTestquestionsMapper extends BaseMapper<TestpaperTestquestions> {

}