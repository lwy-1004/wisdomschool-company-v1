package cn.wstom.onlineexam.mapper;

import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.CoursepaperCoursequestions;

/**
 * 试卷做的题目答案 数据层
 *
 * @author hzh
 * @date 20190223
 */
public interface CoursepaperCoursequestionsMapper extends BaseMapper<CoursepaperCoursequestions> {

}