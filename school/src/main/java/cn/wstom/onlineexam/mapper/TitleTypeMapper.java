package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TitleType;


public interface TitleTypeMapper extends BaseMapper<TitleType> {

}
