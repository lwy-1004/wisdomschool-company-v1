package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.TestStuoptionanswer;

/**
* 学生答案（节测试） 数据层
*
* @author dws
* @date 20190308
*/
public interface TestStuoptionanswerMapper extends BaseMapper<TestStuoptionanswer> {

    int updateByIdAns(TestStuoptionanswer testStuoptionanswer);

}