package cn.wstom.onlineexam.mapper;


import cn.wstom.common.base.mapper.BaseMapper;
import cn.wstom.onlineexam.entity.MyOptionAnswer;


public interface MyOptionAnswerMapper extends BaseMapper<MyOptionAnswer> {


}