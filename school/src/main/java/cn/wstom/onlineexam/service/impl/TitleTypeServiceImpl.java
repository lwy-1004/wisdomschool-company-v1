package cn.wstom.onlineexam.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.TitleType;
import cn.wstom.onlineexam.mapper.TitleTypeMapper;
import cn.wstom.onlineexam.service.TitleTypeService;
import org.springframework.stereotype.Service;


@Service
public class TitleTypeServiceImpl extends BaseServiceImpl<TitleTypeMapper, TitleType> implements TitleTypeService {

}
