package cn.wstom.onlineexam.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.common.constant.Constants;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.mapper.*;
import cn.wstom.onlineexam.service.StuOptionExamanswerService;
import cn.wstom.onlineexam.service.StuOptionanswerService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 学生考试答案 服务层实现
 *
 * @author hzh
 * @date 20190304
 */
@Service
public class StuOptionExamanswerServiceImpl extends BaseServiceImpl<StuOptionExamanswerMapper, StuOptionExamanswer>
        implements StuOptionExamanswerService {

    @Autowired
    private StuOptionExamanswerMapper stuOptionanswerMapper;
    @Autowired
    private UserExamMapper userExamMapper;

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private TestPaperOneMapper testPaperOneMapper;

    @Override
    public int updateByIdAns(StuOptionExamanswer stuOptionanswer) {
        return stuOptionanswerMapper.updateByIdAns(stuOptionanswer);
    }

    @Transactional
    @Override
    public int saveOptionAnswers(List<OptionExamSubmitVo> options, String studentId) {
        List<StuOptionExamanswer> optionanswers = new ArrayList<>();
        Map<String, StuOptionExamanswer> updateMap = new HashMap<>();

        SysUser studentUser = sysUserMapper.selectUserByUserAttrId(studentId);

//        if (options.isEmpty()) {
//            throw new Exception();
//        }

        //  获取已记录的做题数据
        if (options.get(0) != null) {
            StuOptionExamanswer updateEntity = new StuOptionExamanswer();
            System.out.println(studentUser.toString());
            updateEntity.setCreateId(Integer.valueOf(studentUser.getId()));
            updateEntity.setPaperOneId(Integer.valueOf(options.get(0).getTestPaperOneId()));
            List<StuOptionExamanswer> updateList = stuOptionanswerMapper.selectList(updateEntity);
            updateList.forEach(ue -> {
                updateMap.put(ue.getTestpaperOptionanswer(), ue);
            });
        }


        options.forEach(option -> {
            //  动态计算可判断的题目分数
            StuOptionExamanswer optionanswer = new StuOptionExamanswer();
            //  若不存在则插入数据
            if (updateMap.get(option.getTestPaperQuestionId()) == null) {
                optionanswer.setId(option.getuUid());
                optionanswer.setCreateId(Integer.valueOf(studentUser.getId()));
                optionanswer.setPaperOneId(Integer.valueOf(option.getTestPaperOneId()));
                optionanswer.setStuAnswer(option.getStuAnswer());
                optionanswer.setTestpaperOptionanswer(option.getTestPaperQuestionId());//   题目Id
                optionanswer.setStoption(option.getStuOptionAnswer());
                optionanswer.setQuestionScore(option.getScore());
                optionanswers.add(optionanswer);
            } else {
                // 若存在则更新数据
                optionanswer = updateMap.get(option.getTestPaperQuestionId());
                optionanswer.setStuAnswer(option.getStuAnswer());
                optionanswer.setStoption(option.getStuOptionAnswer());
                optionanswer.setQuestionScore(option.getScore());
                stuOptionanswerMapper.update(optionanswer);
            }
        });

        TestPaperOne testPaper = testPaperOneMapper.selectById(options.get(0).getTestPaperOneId());
        //  更新提交状态
        UserExam userExam = new UserExam();
        userExam.setTestPaperOneId(options.get(0).getTestPaperOneId());
        userExam.setUserId(studentUser.getId());
        List<UserExam> index = userExamMapper.selectListBase(userExam);
        if (index.isEmpty()) {
            userExam.setCreateId(studentUser.getId());
            userExam.setcId(testPaper.getCoursrId());
            userExamMapper.insert(userExam);
        }else {
            userExam.setId(index.get(0).getId());
        }
        userExam.setSumbitState(Constants.EXAM_SUBMIT_SCORE_DONE);

        if (!optionanswers.isEmpty()) {
            stuOptionanswerMapper.insertBatch(optionanswers);
        }

        return userExamMapper.update(userExam);
    }

    @Transactional
    @Override
    public int saveOptionAnswersByUserId(List<OptionExamSubmitVo> options, String userId) {
        List<StuOptionExamanswer> optionanswers = new ArrayList<>();
        Map<String, StuOptionExamanswer> updateMap = new HashMap<>();

        //  获取已记录的做题数据
        if (options.get(0) != null) {
            StuOptionExamanswer updateEntity = new StuOptionExamanswer();
            updateEntity.setCreateId(Integer.valueOf(userId));
            updateEntity.setPaperOneId(Integer.valueOf(options.get(0).getTestPaperOneId()));
            List<StuOptionExamanswer> updateList = stuOptionanswerMapper.selectList(updateEntity);
            updateList.forEach(ue -> {
                updateMap.put(ue.getTestpaperOptionanswer(), ue);
            });
        }


        options.forEach(option -> {
            //  动态计算可判断的题目分数
            StuOptionExamanswer optionanswer = new StuOptionExamanswer();
            //  若不存在则插入数据
            if (updateMap.get(option.getTestPaperQuestionId()) == null) {
                optionanswer.setId(option.getuUid());
                optionanswer.setCreateId(Integer.valueOf(userId));
                optionanswer.setPaperOneId(Integer.valueOf(option.getTestPaperOneId()));
                optionanswer.setStuAnswer(option.getStuAnswer());
                optionanswer.setTestpaperOptionanswer(option.getTestPaperQuestionId());//   题目Id
                optionanswer.setStoption(option.getStuOptionAnswer());
                optionanswer.setQuestionScore(option.getScore());
                optionanswers.add(optionanswer);
            } else {
                // 若存在则更新数据
                optionanswer = updateMap.get(option.getTestPaperQuestionId());
                optionanswer.setStuAnswer(option.getStuAnswer());
                optionanswer.setStoption(option.getStuOptionAnswer());
                optionanswer.setQuestionScore(option.getScore());
                stuOptionanswerMapper.update(optionanswer);
            }
        });

        TestPaperOne testPaper = testPaperOneMapper.selectById(options.get(0).getTestPaperOneId());
        //  更新提交状态
        UserExam userExam = new UserExam();
        userExam.setTestPaperOneId(options.get(0).getTestPaperOneId());
        userExam.setUserId(userId);
        List<UserExam> index = userExamMapper.selectListBase(userExam);
        if (index.isEmpty()) {
            userExam.setCreateId(userId);
            userExam.setcId(testPaper.getCoursrId());
            userExamMapper.insert(userExam);
        } else {
            userExam.setId(index.get(0).getId());
        }
        userExam.setSumbitState(Constants.EXAM_SUBMIT_SCORE_DONE);

        if (!optionanswers.isEmpty()) {
            stuOptionanswerMapper.insertBatch(optionanswers);
        }

        return userExamMapper.update(userExam);
    }

    @Transactional
    @Override
    public int updateListAndTotalScore(List<StuOptionExamanswer> optionanswers, String testPaperOneId, String userId) {

        AtomicInteger totalScore = new AtomicInteger();
        try {
            //  更新题目得分
            optionanswers.forEach(o -> {
                stuOptionanswerMapper.update(o);
                totalScore.addAndGet(o.getQuestionScore());
            });
            UserExam userExam = new UserExam();

            //  更新总分
            userExam.setTestPaperOneId(testPaperOneId);
            userExam.setUserId(userId);
            UserExam index = userExamMapper.selectListBase(userExam).get(0);

            userExam.setStuScore(String.valueOf(totalScore));
            userExam.setMadeScore(Constants.EXAM_MADE_SCORE_DONE);
            userExam.setId(index.getId());
            userExamMapper.update(userExam);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

}
