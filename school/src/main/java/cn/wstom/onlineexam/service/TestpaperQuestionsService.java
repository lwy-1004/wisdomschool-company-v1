package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.TestpaperQuestions;

import java.util.List;

/**
 * 试卷题目 服务层
 *
 * @author hzh
 * @date 20190223
 */
public interface TestpaperQuestionsService extends BaseService<TestpaperQuestions> {
    List<TestpaperQuestions> selectListBase(TestpaperQuestions testpaperQuestions);
    TestpaperQuestions selectByPersonalQuestionId(Integer perQueId);
}
