package cn.wstom.onlineexam.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.MyTitleType;
import cn.wstom.onlineexam.mapper.MyTitleTypeMapper;
import cn.wstom.onlineexam.service.MyTitleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* 我的题型 服务层实现
*
* @author dws
* @date 20190307
*/
@Service
public class MyTitleTypeServiceImpl extends BaseServiceImpl
        <MyTitleTypeMapper, MyTitleType>
implements MyTitleTypeService {

@Autowired
private MyTitleTypeMapper myTitleTypeMapper;

public List<MyTitleType> findByCid(String id){
    return myTitleTypeMapper.selectByCId(id);

}
    public List<MyTitleType> findByCidAndTid(String cid,String tid){
    return myTitleTypeMapper.selectByCIdAndTId(cid ,tid);
    }
    @Override
    public List<MyTitleType> findTitleTypeLimit(MyTitleType myTitleType) {
        return myTitleTypeMapper.findTitleTypeLimit(myTitleType);
    }


}
