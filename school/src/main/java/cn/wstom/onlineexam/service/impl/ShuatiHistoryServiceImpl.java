package cn.wstom.onlineexam.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.ShuatiHistory;
import cn.wstom.onlineexam.mapper.ShuatiHistoryMapper;
import org.springframework.stereotype.Service;
import cn.wstom.onlineexam.service.ShuatiHistoryService;

import javax.annotation.Resource;


@Service
public class ShuatiHistoryServiceImpl extends BaseServiceImpl<ShuatiHistoryMapper, ShuatiHistory> implements ShuatiHistoryService {
    @Resource
    private ShuatiHistoryMapper shuatiHistoryMapper;

    /**
     * 通过用户ID查询
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public ShuatiHistory selectByUserId(String userId) {
        return shuatiHistoryMapper.selectByUserId(userId);
    }
}

