package cn.wstom.onlineexam.service.impl;



import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.Websocket;
import cn.wstom.onlineexam.mapper.WebsocketMapper;
import cn.wstom.onlineexam.service.WebsocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
* 存放考试的临时 服务层实现
*
* @author dws
* @date 20190308
*/
@Service
public class WebsocketServiceImpl extends BaseServiceImpl
        <WebsocketMapper, Websocket>
implements WebsocketService {

@Autowired
private WebsocketMapper websocketMapper;
}
