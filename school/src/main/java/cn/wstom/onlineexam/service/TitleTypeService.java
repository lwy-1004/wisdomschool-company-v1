package cn.wstom.onlineexam.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.TitleType;

/**
 * 题型
 *
 * @author hzh
 * @date 20190223
 */
public interface TitleTypeService extends BaseService<TitleType> {



}
