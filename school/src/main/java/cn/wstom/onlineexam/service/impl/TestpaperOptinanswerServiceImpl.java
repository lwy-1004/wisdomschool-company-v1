package cn.wstom.onlineexam.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.TestpaperOptinanswer;
import cn.wstom.onlineexam.mapper.TestpaperOptinanswerMapper;
import cn.wstom.onlineexam.service.TestpaperOptinanswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 试卷答案 服务层实现
 *
 * @author hzh
 * @date 20190223
 */
@Service
public class TestpaperOptinanswerServiceImpl extends BaseServiceImpl<TestpaperOptinanswerMapper, TestpaperOptinanswer>
        implements TestpaperOptinanswerService {

    @Autowired
    private TestpaperOptinanswerMapper testpaperOptinanswerMapper;
}
