package cn.wstom.onlineexam.service;


import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.MyQuestions;

import java.util.List;

public interface MyQuestionsService extends BaseService<MyQuestions> {
    /**
     * 按条件查询问题
     */
    List<MyQuestions> selectList(MyQuestions myQuestions);

    /**
     * 查询未整理的代码
     * @param myQuestions
     * @return
     */
    List<MyQuestions> unList(MyQuestions myQuestions);
    /**
     * 由Id查询未整理的代码
     * @param id
     * @return
     */
    MyQuestions getUnListById(String id);

    /**
     * 查询一门科目中的一种题型的全部试题
     *
     * @param titleTypeId
     * @param xzsubjectsId
     * @return
     */
    List<MyQuestions> selectQuestionByTid(String titleTypeId, String xzsubjectsId);

}

