package cn.wstom.onlineexam.service;

import cn.wstom.common.base.service.BaseService;
import cn.wstom.onlineexam.entity.StuQuestionScore;

/**
* 分数 服务层
*
* @author hzh
* @date 20190307
*/
public interface StuQuestionScoreService extends BaseService<StuQuestionScore> {
}
