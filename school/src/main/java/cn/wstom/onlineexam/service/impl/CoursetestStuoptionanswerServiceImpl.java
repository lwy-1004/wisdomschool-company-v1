package cn.wstom.onlineexam.service.impl;


import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.CoursetestStuoptionanswer;
import cn.wstom.onlineexam.mapper.CoursetestStuoptionanswerMapper;
import cn.wstom.onlineexam.service.CoursetestStuoptionanswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 学生答案（课程测试） 服务层实现
 *
 * @author dws
 * @date 20190315
 */
@Service
public class CoursetestStuoptionanswerServiceImpl extends BaseServiceImpl
        <CoursetestStuoptionanswerMapper, CoursetestStuoptionanswer>
        implements CoursetestStuoptionanswerService {

    @Autowired
    private CoursetestStuoptionanswerMapper coursetestStuoptionanswerMapper;

    @Override
    public int updateByIdAns(CoursetestStuoptionanswer coursetestStuoptionanswer) {
        return coursetestStuoptionanswerMapper.updateByIdAns(coursetestStuoptionanswer);
    }
}
