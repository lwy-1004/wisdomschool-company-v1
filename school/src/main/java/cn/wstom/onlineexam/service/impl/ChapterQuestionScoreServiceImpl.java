package cn.wstom.onlineexam.service.impl;



import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.ChapterQuestionScore;
import cn.wstom.onlineexam.mapper.ChapterQuestionScoreMapper;
import cn.wstom.onlineexam.service.ChapterQuestionScoreService;
import org.springframework.stereotype.Service;


/**
* 分数 服务层实现
*
* @author hzh
* @date 20190307
*/
@Service
public class ChapterQuestionScoreServiceImpl extends BaseServiceImpl
        <ChapterQuestionScoreMapper, ChapterQuestionScore>
implements ChapterQuestionScoreService {

}
