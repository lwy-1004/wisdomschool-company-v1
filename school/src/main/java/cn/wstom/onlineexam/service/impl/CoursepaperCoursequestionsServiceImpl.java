package cn.wstom.onlineexam.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.CoursepaperCoursequestions;
import cn.wstom.onlineexam.mapper.CoursepaperCoursequestionsMapper;
import cn.wstom.onlineexam.service.CoursepaperCoursequestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 试卷做的题目答案 服务层实现
 *
 * @author hzh
 * @date 20190223
 */
@Service
public class CoursepaperCoursequestionsServiceImpl extends BaseServiceImpl<CoursepaperCoursequestionsMapper, CoursepaperCoursequestions>
        implements CoursepaperCoursequestionsService {

    @Autowired
    private CoursepaperCoursequestionsMapper coursepaperCoursequestionsMapper;

}
