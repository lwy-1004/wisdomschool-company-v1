package cn.wstom.onlineexam.service.impl;



import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.PublicOptionAnswer;
import cn.wstom.onlineexam.mapper.PublicOptionAnswerMapper;
import cn.wstom.onlineexam.service.PublicOptionAnswerService;
import org.springframework.stereotype.Service;

@Service
public class PublicOptionAnswerServiceImpl extends BaseServiceImpl<PublicOptionAnswerMapper, PublicOptionAnswer> implements PublicOptionAnswerService {



}
