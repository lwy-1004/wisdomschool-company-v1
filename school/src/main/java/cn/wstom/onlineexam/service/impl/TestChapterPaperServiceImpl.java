package cn.wstom.onlineexam.service.impl;

import cn.wstom.common.base.service.impl.BaseServiceImpl;
import cn.wstom.onlineexam.entity.TestChapterPaper;
import cn.wstom.onlineexam.mapper.TestChapterPaperMapper;
import cn.wstom.onlineexam.service.TestChapterPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 节的试卷 服务层实现
*
* @author dws
* @date 20190314
*/
@Service
public class TestChapterPaperServiceImpl extends BaseServiceImpl
        <TestChapterPaperMapper, TestChapterPaper>
implements TestChapterPaperService {

@Autowired
private TestChapterPaperMapper testChapterPaperMapper;
}
