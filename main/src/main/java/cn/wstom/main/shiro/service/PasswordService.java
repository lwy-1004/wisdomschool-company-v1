package cn.wstom.main.shiro.service;

import cn.wstom.common.constant.Constants;
import cn.wstom.main.async.AsyncManager;
import cn.wstom.main.async.factory.AsyncFactory;
import cn.wstom.main.util.MessageUtils;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.exception.user.UserPasswordNotMatchException;
import cn.wstom.main.web.exception.user.UserPasswordRetryLimitExceedException;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.mapper.SysUserMapper;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 登录密码方法
 *
 * @author dws
 */
@Component
public class PasswordService {
    @Autowired
    private CacheManager cacheManager;

    private Cache<String, AtomicInteger> loginRecordCache;

    @Value(value = "${user.password.maxRetryCount}")
    private int maxRetryCount;

    @Autowired
    private SysUserMapper sysUserMapper;

    public static void main(String[] args) {
        System.out.println(new PasswordService().encryptPassword("1001", "123456", "9010ee"));
        System.out.println(ShiroUtils.encryptPassword("1002", "123456", "0f234e"));
    }

    @PostConstruct
    public void init() {
        loginRecordCache = cacheManager.getCache("loginRecordCache");
    }

    public void validate(SysUser sysUser, String password) {
        String loginName = sysUser.getLoginName();

        AtomicInteger retryCount = loginRecordCache.get(loginName);

        if (retryCount == null) {
            retryCount = new AtomicInteger(0);
            loginRecordCache.put(loginName, retryCount);
        }
        if (retryCount.incrementAndGet() > maxRetryCount) {
            AsyncManager.async().execute(AsyncFactory.recordLoginInfo(loginName, Constants.LOGIN_FAIL, MessageUtils.message("sysUser.password.retry.limit.exceed", maxRetryCount)));
            throw new UserPasswordRetryLimitExceedException(maxRetryCount);
        }

        if (!matches(sysUser, password)) {
            AsyncManager.async().execute(AsyncFactory.recordLoginInfo(loginName, Constants.LOGIN_FAIL, MessageUtils.message("sysUser.password.retry.limit.count", retryCount)));
            loginRecordCache.put(loginName, retryCount);
            throw new UserPasswordNotMatchException();
        } else {
            clearLoginRecordCache(loginName);
        }
    }

    public boolean matches(SysUser sysUser, String newPassword) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("loginName", sysUser.getLoginName());
        params.put("password", encryptPassword(sysUser.getLoginName(), newPassword, sysUser.getSalt()));
        return sysUserMapper.selectCount(params) == 1;
        //return sysUser.getPassword().equals(encryptPassword(sysUser.getLoginName(), newPassword, sysUser.getSalt()));
    }

    public void clearLoginRecordCache(String username) {
        loginRecordCache.remove(username);
    }

    public String encryptPassword(String username, String password, String salt) {
        return ShiroUtils.encryptPassword(username, password, salt);
    }
}
