package cn.wstom.main.shiro.service;

import cn.wstom.common.constant.Constants;
import cn.wstom.common.constant.ShiroConstants;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.enums.UserStatus;
import cn.wstom.common.utils.DateUtils;
import cn.wstom.main.async.AsyncManager;
import cn.wstom.main.async.factory.AsyncFactory;
import cn.wstom.main.util.MessageUtils;
import cn.wstom.main.util.ServletUtils;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.exception.user.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 登录校验方法
 *
 * @author dws
 */
@Component
public class LoginService {

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private SysUserService sysUserService;

    /**
     * 登录
     */
    public SysUser login(String username, String password) throws Exception {
        // 验证码校验
        if (!StringUtils.isEmpty(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA))) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "sysUser.jcaptcha.error");
            throw new CaptchaException();
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "not.null");
            throw new UserNotExistsException();
        }
        // 用户名或密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH
                || username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "sysUser.password.not.match");
            throw new UserPasswordNotMatchException();
        }

        // 查询用户信息
        SysUser sysUser = sysUserService.selectUserByLoginName(username);

        if (sysUser == null && loginByPhone(username)) {
            sysUser = sysUserService.selectUserByPhoneNumber(username);
        }

        if (sysUser == null && loginByEmail(username)) {
            sysUser = sysUserService.selectUserByEmail(username);
        }

        if (sysUser == null) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "sysUser.not.exists");
            throw new UserNotExistsException();
        }

        if (UserStatus.DELETED.getCode().equals(sysUser.getDelFlag())) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "sysUser.password.delete");
            throw new UserDeleteException();
        }

        if (UserStatus.DISABLE.getCode().equals(sysUser.getStatus())) {
            recordLoginInfo(username, Constants.LOGIN_FAIL, "sysUser.blocked", sysUser.getRemark());
            throw new UserBlockedException(sysUser.getRemark());
        }

        passwordService.validate(sysUser, password);

        recordLoginInfo(username, Constants.LOGIN_SUCCESS, "sysUser.login.success");
        sysUser.setLoginIp(ShiroUtils.getIp());
        sysUser.setLoginDate(DateUtils.getNowDate());
        sysUserService.update(sysUser);
        return sysUser;
    }

    private boolean loginByEmail(String username) {
        return username.matches(UserConstants.EMAIL_PATTERN);
    }

    private boolean loginByPhone(String username) {
        return username.matches(UserConstants.MOBILE_PHONE_NUMBER_PATTERN);
    }

    /**
     * 记录登录信息
     */
    private void recordLoginInfo(String username, String status, String msgCode, Object... args) {
        AsyncManager.async().execute(AsyncFactory.recordLoginInfo(username, status, MessageUtils.message(msgCode, args)));
    }
}
