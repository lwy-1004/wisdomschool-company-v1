package cn.wstom.main.shiro.realm;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;

/**
 * 自定义Realm 处理登录 权限
 *
 * @author dws
 */
public abstract class Realm extends AuthorizingRealm {
    /**
     * 清理缓存权限
     */
    public abstract void clearCachedAuthorizationInfo();
}
