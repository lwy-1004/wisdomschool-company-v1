package cn.wstom.shiro;

import cn.wstom.common.constant.UserConstants;
import cn.wstom.jiaowu.data.SdsadminVo;
import cn.wstom.jiaowu.service.DepartmentService;
import cn.wstom.jiaowu.service.GradesService;
import cn.wstom.jiaowu.service.SdsadminVoService;
import cn.wstom.main.shiro.realm.Realm;
import cn.wstom.main.shiro.service.LoginService;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.exception.user.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysMenuService;
import cn.wstom.system.service.SysRoleService;
import cn.wstom.web.event.WebUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserRealm extends Realm {
    private static final Logger log = LoggerFactory.getLogger(UserRealm.class);
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private SdsadminVoService sdsadminVoService;
    @Autowired
    private GradesService gradesService;
    @Autowired
    private DepartmentService departmentService;
    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection arg0) {
        SysUser sysUser = ShiroUtils.getUser();
        if(sysUser.getUserType().equals(UserConstants.USER_SCHOOL_ADMIN)){
            System.out.println(sysUser.getUserAttrId());
//            SdsadminVo sdsadminVo= sdsadminVoService.getById(sysUser.getUserAttrId());
//            System.out.println(sdsadminVo);
//            int g= sdsadminVo.getGrades();
//            int d=sdsadminVo.getDepartment();
//            String gradename = (gradesService.getById(g)).getName();
//            String departmentname=(departmentService.getById(d)).getName();
//            WebUtils.getSession().setAttribute("gradename", gradename);
//            WebUtils.getSession().setAttribute("departmentname", departmentname);
//            WebUtils.getSession().setAttribute("did",d);
        }

        // 角色列表
        Set<String> roles;
        // 功能列表
        Set<String> menus;
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 管理员拥有所有权限
        if (UserConstants.ADMIN_ID.equals(sysUser.getId())) {
            info.addRole("admin");
            info.addStringPermission("*:*:*");
        } else {
            roles = roleService.selectRoleKeys(sysUser.getId());
            menus = menuService.selectPermsByUserId(sysUser.getId());
            // 角色加入AuthorizationInfo认证对象
            info.setRoles(roles);
            // 权限加入AuthorizationInfo认证对象
            info.setStringPermissions(menus);
        }
        return info;
    }

    /**
     * 登录认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        String username = upToken.getUsername();
        String password = StringUtils.EMPTY;
        if (upToken.getPassword() != null) {
            password = new String(upToken.getPassword());
        }

        SysUser sysUser = null;
        try {
            sysUser = loginService.login(username, password);
        } catch (CaptchaException e) {
            throw new AuthenticationException(e.getMessage(), e);
        } catch (UserNotExistsException e) {
            throw new UnknownAccountException(e.getMessage(), e);
        } catch (UserPasswordNotMatchException e) {
            throw new IncorrectCredentialsException(e.getMessage(), e);
        } catch (UserPasswordRetryLimitExceedException e) {
            throw new ExcessiveAttemptsException(e.getMessage(), e);
        } catch (UserBlockedException | RoleBlockedException e) {
            throw new LockedAccountException(e.getMessage(), e);
        } catch (Exception e) {
            log.info("对用户[" + username + "]进行登录验证..验证未通过{}", e.getMessage());
            throw new AuthenticationException(e.getMessage(), e);
        }
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(sysUser, password, getName());
        return info;
    }

    /**
     * 清理缓存权限
     */
    @Override
    public void clearCachedAuthorizationInfo() {
        this.clearCachedAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());
    }
}