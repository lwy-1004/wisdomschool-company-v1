package cn.wstom.web.directive;

import cn.wstom.main.template.DirectiveHandler;
import cn.wstom.main.template.directive.BaseTemplateDirective;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.square.data.PageVo;
import cn.wstom.square.entity.Feed;
import cn.wstom.square.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author dws
 * @date 2019/03/09
 */
@Component
public class FeedPageDirective extends BaseTemplateDirective {
    private final FeedService feedService;
    private static final String RESULT_KEY = "feed";

    @Autowired
    public FeedPageDirective(FeedService feedService) {
        this.feedService = feedService;
    }

    @Override
    public String getName() {
        return "feedPage";
    }

    @Override
    public void execute(DirectiveHandler handler) throws Exception {

        // 获取页面的参数
        //所属主信息类型，0是所有，1是文章，2是小组话题

        String userId = handler.getBoolean("personal", false) ? ShiroUtils.getUserId() : null;
        Integer infoType = handler.getInteger("infoType", 0);
        //翻页页数
        Integer p = handler.getInteger("p", 1);
        //每页记录条数
        Integer rows = handler.getInteger("rows", 10);
        // 获取文件的分页
        try {
            PageVo<Feed> pageVo = feedService.getUserListFeedPage(userId, infoType, p, rows);
            System.out.println(pageVo.getList());
            handler.put(RESULT_KEY, pageVo).render();
        } catch (Exception e) {
            e.printStackTrace();
            handler.put(MSG, e.getMessage()).render();
        }
    }
}
