package cn.wstom.web.directive;

import cn.wstom.main.Environment;
import cn.wstom.main.cache.CacheCell;
import cn.wstom.main.cache.CacheItem;
import cn.wstom.main.cache.CacheManager;
import cn.wstom.main.template.DirectiveHandler;
import cn.wstom.main.template.directive.BaseTemplateDirective;
import cn.wstom.system.entity.SysDictData;
import cn.wstom.system.service.SysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 数据字典指令
 *
 * @author dws
 * @date 2018/12/22
 */
@Component
public class DictionaryDirective extends BaseTemplateDirective {

	@Autowired
	private SysDictDataService dictDataService;

	@Override
	public String getName() {
		return "dictionary";
	}

	@Override
    public void execute(DirectiveHandler handler) throws Exception {
		String dictType = handler.getString("dictType");
        CacheManager cacheManager = (CacheManager) Environment.getCacheManager();
        Map<String, ?> item = cacheManager.getAllItem(CacheCell.CACHE_DICT);
        CacheItem<LinkedHashMap<String, SysDictData>> dictData =
                (CacheItem<LinkedHashMap<String, SysDictData>>) item.get(dictType);
        handler.put("dictData", dictData.getItem().values()).render();
	}
}
