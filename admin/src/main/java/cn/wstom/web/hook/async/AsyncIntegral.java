package cn.wstom.web.hook.async;

import cn.wstom.jiaowu.service.IntegralDetailService;
import cn.wstom.main.util.SpringUtils;
import cn.wstom.system.entity.SysUser;

import java.util.TimerTask;

/**
 * @author dws
 * @date 2019/03/28
 */
public class AsyncIntegral {


    /**
     * 积分记录
     *
     * @param content 积分内容
     * @return 任务task
     */
    public static TimerTask recordIntegral(SysUser user, String code, String content) {
        IntegralDetailService detailService = SpringUtils.getBean(IntegralDetailService.class);
        return new TimerTask() {
            @Override
            public void run() {
                try {
                    detailService.handleCedit(user, code, content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
