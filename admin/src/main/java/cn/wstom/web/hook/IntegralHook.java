package cn.wstom.web.hook;

import cn.wstom.main.hook.interceptor.BaseInterceptorHookSupport;
import cn.wstom.web.pugin.IntegralConfig;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author dws
 * @date 2019/03/28
 */
@Component
public class IntegralHook extends BaseInterceptorHookSupport {

    @Override
    public void init() {
        this.plugins = getPlugins(IntegralHook.IntegralInterceptorListener.class);
    }

    @Override
    public String[] getInterceptor() {
        //说明要拦截的controller
        return IntegralConfig.getCondition().keySet().toArray(new String[]{});
    }

    @Override
    public void preHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler) throws Exception {
        super.onPreHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, ModelAndView modelAndView) throws Exception {
        super.onPostHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, Exception ex) throws Exception {
        super.onAfterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler) throws Exception {
        super.onAfterConcurrentHandlingStarted(request, response, handler);
    }

    public interface IntegralInterceptorListener extends InterceptorListener {

    }
}
