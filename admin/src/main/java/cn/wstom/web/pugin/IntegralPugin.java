package cn.wstom.web.pugin;

import cn.wstom.web.hook.IntegralHook;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 积分插件
 *
 * @author dws 2015/10/31
 */
@Component
public class IntegralPugin implements IntegralHook.IntegralInterceptorListener {

    @Override
    public void preHandle(HttpServletRequest request,
                          HttpServletResponse response,
                          HandlerMethod handler) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           HandlerMethod handler,
                           ModelAndView modelAndView) throws Exception {

        System.out.println("***************积分处理***************");

        /*String message = handler.getShortLogMessage();
        String s = IntegralConfig.getCondition().get(message.substring(0, message.lastIndexOf("[")));
        StringBuilder sb = new StringBuilder();
        if ("article_read".equals(s)) {
            Article article = (Article) modelAndView.getModel().get("article");
            sb.append("阅读文章【").append(article.getTitle()).append("】");
        } else if ("article_edit".equals(s)) {
            sb.append("阅读文章【").append(article.getTitle()).append("】");
        }
        "article_read");
        //添加文章
        condition.put("cn.wstom.web.controller.front.ArticleController#save", "article_edit");
        //评论文章
        condition.put("cn.wstom.web.controller.front.ArticleController#comment", "article_comment");
        //课程讨论
        condition.put("cn.wstom.web.controller.front.StudentController#comment", "course_comment");
        //观看视频
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#playVideo", "video_view");
        //下载课件
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#downloadFile", "download_file");
        //预览课件
        condition.put("cn.wstom.web.controller.school.teacher.RecourseController#review", "file_review");
        //提问
        condition.put("cn.wstom.web.controller.front.QuestionController#addQuestion", "question_editor");
        //回复问题
        condition.put("cn.wstom.web.controller.front.QuestionController#addAnswer", "answer_editor");
        AsyncManager.async().execute(
                AsyncIntegral.recordIntegral(ShiroUtils.getUser(), s, modelAndView.getModel().toString()));*/
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                HandlerMethod handler,
                                Exception ex) throws Exception {

    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request,
                                               HttpServletResponse response,
                                               HandlerMethod handler) throws Exception {

    }
}
