package cn.wstom.web.pugin;

import cn.wstom.square.entity.Article;
import cn.wstom.web.hook.controllerHook.ArticleControllerHook;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 版权插件
 * @author dws 2015/10/31
 */
@Component
public class ViewCopyrightPugin implements ArticleControllerHook.ArticleControllerInterceptorListener {

    @Override
    public void preHandle(HttpServletRequest request,
                          HttpServletResponse response,
                          HandlerMethod handler) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           HandlerMethod handler,
                           ModelAndView modelAndView) throws Exception {
        Article ret = (Article) modelAndView.getModelMap().get("article");
        if (ret != null) {
            StringBuilder sb = new StringBuilder(ret.getContent());
            String str = "注意：本文归作者所有，未经作者允许，不得转载";
            if (!str.equals(sb.toString())) {
                sb.append("<br/><p style='text-align: center;'>注意：本文归作者所有，未经作者允许，不得转载</p>");
                ret.setContent(sb.toString());
            }
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                HandlerMethod handler,
                                Exception ex) throws Exception {

    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler) throws Exception {

    }
}
