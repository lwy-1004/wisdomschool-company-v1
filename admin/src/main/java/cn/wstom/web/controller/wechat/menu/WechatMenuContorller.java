package cn.wstom.web.controller.wechat.menu;

import cn.wstom.common.base.Data;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.wechat.module.menu.service.WechatMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 微信公众号菜单创建控制器
 * @author LK
 * @create 2019/9/22 23:54
 **/
@RestController
@RequestMapping("/wx/menu/{appId}")
@Slf4j
public class WechatMenuContorller extends BaseController {

    @Autowired
    private WechatMenuService wechatMenuService;

    /**
     * 执行生成按钮
     * @param appId
     * @return
     */
    @GetMapping("/execute")
    public Data menuExecute(@PathVariable String appId) {
        try {
            boolean result = wechatMenuService.executeMenu(appId);
            if (result) {
                return success("微信按钮执行成功！");
            }
        } catch (Exception e) {
            log.error("【微信服务】微信菜单：创建失败,e={}", e);
        }
        return error();
    }
}
