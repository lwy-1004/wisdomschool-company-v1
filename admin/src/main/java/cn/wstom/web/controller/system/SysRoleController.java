package cn.wstom.web.controller.system;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.support.Convert;
import cn.wstom.main.util.ExcelUtil;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.system.entity.SysRole;
import cn.wstom.system.service.SysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 角色信息
 *
 * @author dws
 */
@Controller
@RequestMapping("/system/role")
public class SysRoleController extends BaseController {
    private String prefix = "system/role";

    @Autowired
    private SysRoleService roleService;

    @RequiresPermissions("system:role:view")
    @GetMapping()
    public String toList() {
        return prefix + "/list";
    }

    //    @RequiresPermissions("system:role:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysRole role) {
        startPage();
        List<SysRole> list = roleService.list(role);
        return wrapTable(list);
    }

    @Log(title = "角色管理", actionType = ActionType.EXPORT)
    @RequiresPermissions("system:role:export")
    @PostMapping("/export")
    @ResponseBody
    public Data export(SysRole role) {
        try {
            List<SysRole> list = roleService.list(role);
            ExcelUtil<SysRole> util = new ExcelUtil<>(SysRole.class);
            return util.exportExcel(list, "role");
        } catch (Exception e) {
            return Data.error(e.getMessage());
        }
    }

    /**
     * 新增角色
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存角色
     */
    @RequiresPermissions("system:role:add")
    @Log(title = "角色管理", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addSave(SysRole role) throws Exception {
        role.setCreateBy(ShiroUtils.getLoginName());
        role.getMenuIds();
        boolean save = roleService.save(role) && roleService.updateRoleMenu(role) > 0;
        ShiroUtils.clearCachedAuthorizationInfo();
        return toAjax(save);
    }

    /**
     * 修改角色
     */
    @GetMapping("/edit/{roleId}")
    public String toEdit(@PathVariable("roleId") String roleId, ModelMap mmap) {
        mmap.put("role", roleService.getById(roleId));
        return prefix + "/edit";
    }

    /**
     * 修改保存角色
     */
    @RequiresPermissions("system:role:edit")
    @Log(title = "角色管理", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data edit(SysRole role) throws Exception {
        role.setUpdateBy(ShiroUtils.getLoginName());
        roleService.update(role);
        boolean save = roleService.update(role) && roleService.updateRoleMenu(role) > 0;
        ShiroUtils.clearCachedAuthorizationInfo();
        return toAjax(save);
    }

    /**
     * 获取权限页
     */
    @GetMapping("/permission/{roleId}")
    public String toPermission(@PathVariable("roleId") String roleId, ModelMap mmap) {
        mmap.put("role", roleService.getById(roleId));
        return prefix + "/permission";
    }

    /**
     * 修改保存数据权限
     */
    @RequiresPermissions("system:role:edit")
    @Log(title = "角色管理", actionType = ActionType.UPDATE)
    @PostMapping("/permission")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data permission(SysRole role) throws Exception {
        role.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(roleService.update(role));
    }

    @RequiresPermissions("system:role:remove")
    @Log(title = "角色管理", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            return toAjax(roleService.removeByIds(idList));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 校验角色名称
     */
    @PostMapping("/checkRoleNameUnique")
    @ResponseBody
    public String checkRoleNameUnique(SysRole role) {
        return roleService.checkRoleNameUnique(role);
    }

    /**
     * 校验角色权限
     */
    @PostMapping("/checkRoleKeyUnique")
    @ResponseBody
    public String checkRoleKeyUnique(SysRole role) {
        return roleService.checkRoleKeyUnique(role);
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree")
    public String selectMenuTree() {
        return prefix + "/tree";
    }
}