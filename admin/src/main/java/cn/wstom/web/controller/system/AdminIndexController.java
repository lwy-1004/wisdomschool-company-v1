package cn.wstom.web.controller.system;

import cn.wstom.common.config.Global;
import cn.wstom.common.constant.UserConstants;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.system.entity.SysMenu;
import cn.wstom.system.entity.SysRole;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysMenuService;
import cn.wstom.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 后台首页 业务处理
 *
 * @author dws
 */
@Controller
@RequestMapping("/admin")
public class AdminIndexController extends BaseController {
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private SysRoleService roleService;
    private String prefix = "/school/";

    /**
     * 后台首页
     * @param modelMap modelMap
     * @return 视图
     */
    @GetMapping({"", "/index"})
    public String index(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
        List<SysMenu> menus = menuService.selectMenusByUser(sysUser);
        modelMap.put("menus", menus);
        modelMap.put("copyrightYear", Global.getCopyrightYear());
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        modelMap.put("roles", roles);
        int usertype=sysUser.getUserType();
        if (usertype==3){
            return prefix + "teacher/select";
        }else {
            return prefix + "index";
        }
//        return prefix + "index";
    }

    @GetMapping( "/teacher")
    public String index2(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
//        List<SysMenu> menus = menuService.selectMenusByUser(sysUser);
        List<SysMenu> menus = menuService.selectMenusByUserId_teacher(sysUser.getId());
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("teacher")) {
                roles.remove(i);
            }
        }
        modelMap.put("nowrole", "教师");
        modelMap.put("roles", roles);
        modelMap.put("menus", menus);
        modelMap.put("copyrightYear", Global.getCopyrightYear());
        return prefix + "index";
    }

    @GetMapping( "/select")
    public String select(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
        List<SysMenu> menus = menuService.selectMenusByUser(sysUser);
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        modelMap.put("roles", roles);
        modelMap.put("menus", menus);
        modelMap.put("copyrightYear", Global.getCopyrightYear());
        return prefix + "teacher/select";
    }

    /*
     *注意这里转到SdsadminRoleController
     * */
    @GetMapping( "/sds_admin")
    public String tosdsadmin(ModelMap modelMap){
        SysUser sysUser = getUser();
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("sds_admin")) {
                roles.remove(i);
            }
        }
        modelMap.put("nowrole", "系部管理员");
        modelMap.put("roles", roles);
        return "/sdsadmin/role/totea_sds";
    }


    /*
     *注意
     * */
    @GetMapping( "/classadmin")
    public String toclassadmin(ModelMap modelMap){
        SysUser sysUser = getUser();
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("classadmin")) {
                roles.remove(i);
            }
        }
        modelMap.put("nowrole", "班主任");
        modelMap.put("roles", roles);
        return "/classadmin/totea_sds";
    }


    /**
     * @param modelMap modelMap
     * @return 视图
     */
    @GetMapping({"/course"})
    public String course(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
        // 根据用户id取出菜单
        /*List<SysMenu> menus = menuService.selectMenusByUser(sysUser);*/
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("teacher")) {
                roles.remove(i);
            }
        }
        System.out.println("course-roles="+roles);
        modelMap.put("nowrole", "教师");
        modelMap.put("roles", roles);
        return prefix + "teacher/index";
    }

    /**
     * @param modelMap modelMap
     * @return 视图
     */
    @GetMapping({"/sdsadmin"})
    public String sdsadmin(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUserId_sdsadmin(sysUser.getId());
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("sds_admin")) {
                roles.remove(i);
            }
        }
        System.out.println("sdsadmin-roles="+roles);
        modelMap.put("nowrole", "系部管理员");
        modelMap.put("roles", roles);
        modelMap.put("menus",menus);
        return prefix + "sdsadmin/index";
    }

    /**
     * @param modelMap modelMap
     * @return 视图
     */
    @GetMapping({"/classadminindex"})
    public String classadmin(ModelMap modelMap) {
        // 取身份信息
        SysUser sysUser = getUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUserId_classadmin(sysUser.getId());
        List<SysRole> roles=roleService.selectAllRolesByUserId(sysUser.getId());
        for(int i = 0;i < roles.size(); i++) {
            String str = roles.get(i).getRoleKey();
            if (str.equals("sds_admin")) {
                roles.remove(i);
            }
        }
        System.out.println("sdsadmin-roles="+roles);
        modelMap.put("nowrole", "班主任");
        modelMap.put("roles", roles);
        modelMap.put("menus",menus);
        return prefix + "classadmin/index";
    }



    /**
     * 系统介绍
     * @param modelMap modelMap
     * @return 视图
     */
    @GetMapping("/system/main")
    public String main(ModelMap modelMap) {
        modelMap.put("version", Global.getVersion());
        return "main";
    }
}
