package cn.wstom.web.controller.front;

import cn.wstom.main.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author dws
 * @date 2019/03/28
 */
@Controller
public class TestJspController extends BaseController {

    @RequestMapping("/test/aaa")
    public String toIndex() {
        return "/index.jsp";
    }

    @RequestMapping("/1aaa")
    public String toIndex1() {
        return "/index.jsp";
    }
}
