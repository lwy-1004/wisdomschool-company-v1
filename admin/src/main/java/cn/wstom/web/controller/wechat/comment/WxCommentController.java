package cn.wstom.web.controller.wechat.comment;

import cn.wstom.common.service.CommentService;
import cn.wstom.main.web.base.BaseController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 微信公众号论坛
 * @author Liniec
 * @create 2019年9月28日 01点07分
 **/
@Controller
@RequestMapping("/wx/comment")
@Slf4j
public class WxCommentController extends BaseController{

    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private CommentService commentService;

    @ApiOperation("论坛首页")
    @GetMapping("index")
    public String index(@RequestParam("openId") String openId, ModelMap modelMap) {
        return "/wx/course/index";
    }

}
