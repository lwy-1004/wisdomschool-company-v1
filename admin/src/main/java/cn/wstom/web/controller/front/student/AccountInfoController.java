package cn.wstom.web.controller.front.student;

import cn.wstom.common.base.Data;
import cn.wstom.common.constant.Constants;
import cn.wstom.common.constant.StorageConstants;
import cn.wstom.common.exception.file.FileNameLimitExceededException;
import cn.wstom.common.utils.FileUtils;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.main.exception.ApplicationException;
import cn.wstom.main.util.file.FileUploadUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author liniec
 * @date 2020/01/28 09:29
 *  学生管理个人话题
 */
@Controller
@RequestMapping("/account/info")
public class AccountInfoController extends BaseController {

    @Autowired
    private SysUserService sysUserService;

    @ApiOperation("修改头像")
    @PostMapping(value = "/thumb", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Data updateThumb(@RequestParam(value = "file") MultipartFile file) throws Exception {
        String filename = null;
        if (!file.isEmpty()) {
            try {
                //保存图片
                Data result = FileUploadUtils.upload(StorageConstants.STORAGE_THUMBNAIL, file, false, FileUtils.allowImage);
                System.out.println(result);
                if (StringUtil.nvl(result.get(Data.RESULT_KEY_CODE).toString(), "").equals(Constants.FAILURE)) {
                    return result;
                }
                filename = (String) result.get(StorageConstants.FILE_ID);
            } catch (FileNameLimitExceededException | IOException | ApplicationException e) {
                return Data.error(e.getMessage());
            }
        }

        SysUser user = getUser();
        user.setAvatar(filename);
        return toAjax(sysUserService.update(user));
    }
}
