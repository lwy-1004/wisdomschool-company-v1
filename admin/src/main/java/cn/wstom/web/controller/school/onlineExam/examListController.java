
package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.base.Data;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * 学生试卷列表、用于教师改卷
 *
 * @author hzh
 * @date 20190223
 */

@Controller
@RequestMapping("/school/onlineExam/examList")
public class examListController extends BaseController {
    private String prefix = "school/onlineExam/examList";

    @Autowired
    private UserTestService userTestService;

    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TestpaperTestquestionsService testpaperTestquestionsService;
    @Autowired
    private TestpaperQuestionsService testpaperQuestionsService;
    @Autowired
    private TestpaperOptinanswerService testpaperOptinanswerService;
    @Autowired
    StuQuestionScoreService stuQuestionScoreService;
    @Autowired
    CoursetestStuoptionanswerService coursetestStuoptionanswerService;
    @Autowired
    CoursepaperCoursequestionsService coursepaperCoursequestionsService;

    @RequiresPermissions("school:onlineExam:course:view")
    @GetMapping("{cid}")
    public String toList(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("cid", cid);
        return prefix + "/list";
    }


    /**
     * 查询试卷列表
     */
    @RequiresPermissions("school:onlineExam:course:view")
    @PostMapping("/list/{cid}")
    @ResponseBody
    public TableDataInfo list(@PathVariable String cid, UserTest userTest) {
        startPage();
        userTest.setcId(cid);
        userTest.setType("1");
        List<UserTest> list = userTestService.findStuExamPaper(userTest);
        if (list.size() != 0) {
            final String done = "是";
            final String doneFlag = "1";
            final String none = "否";
            final String noneFlag = "0";
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
                if (list.get(i).getMadeScore().equals(doneFlag)) {
                    list.get(i).setMadeScore(done);
                } else if(list.get(i).getMadeScore().equals(noneFlag)) {
                    list.get(i).setMadeScore(none);
                }

            }
        }
        return wrapTable(list);
    }

    /**
     * 教师改卷
     */

    @GetMapping("/makeScore")
    public String makeScore(String id,String stuNum, ModelMap mmap) {
        TestPaper testPaper = testPaperService.getById(id);
        SysUser sysUser = new SysUser();
        sysUser.setLoginName(stuNum);
        List<SysUser> sysUserList = sysUserService.list(sysUser);
        String studentId = "";
        if(sysUserList.size()!=0){
            studentId = sysUserList.get(0).getUserAttrId();
        }
        System.out.println("studentId:"+studentId);
        mmap.put("testPaper", testPaper);
        mmap.put("studentId",studentId);
        mmap.put("sysUser",sysUserList.get(0));
        mmap.put("paperId", id);
        return prefix + "/makeScore";
    }


    /**
     * 改卷时初始化页面
     *
     * @return
     */
    @RequestMapping("/startMakePaper")
    @RequiresPermissions("school:onlineExam:course:view")
    @ResponseBody
    public List<TestpaperQuestions> startMakePaper(String paperId, String studentId, ModelMap mmap) throws Exception {
        List<TestpaperQuestions> tqvolist = new ArrayList<TestpaperQuestions>();
        String studentID = studentId;
        TestPaper testPaper =  testPaperService.getById(paperId);
        if(testPaper.getRule().equals("1")){
            System.out.println("课程考试");
            CoursepaperCoursequestions coursepaperCoursequestions = new CoursepaperCoursequestions();
            coursepaperCoursequestions.setTestPaperId(paperId);
            coursepaperCoursequestions.setStudentId(studentId);
            List<CoursepaperCoursequestions> coursepaperCoursequestionsList = new ArrayList<CoursepaperCoursequestions>();
            coursepaperCoursequestionsList = coursepaperCoursequestionsService.list(coursepaperCoursequestions);
            for (int i = 0; i < coursepaperCoursequestionsList.size(); i++) {
                System.out.println("题目数量："+coursepaperCoursequestionsList.size());
                TestpaperQuestions testpaperQuestions = new TestpaperQuestions();
                testpaperQuestions = testpaperQuestionsService.getById(coursepaperCoursequestionsList.get(i).getTestQuestionsId());
                if (testpaperQuestions != null) {
                    System.out.println("加入");
                    tqvolist.add(testpaperQuestions);
                }
            }
        }else {
            TestpaperTestquestions testpaperTestquestions = new TestpaperTestquestions();
            testpaperTestquestions.setTestPaperId(paperId);
            List<TestpaperTestquestions> testpaperTestquestionsList = new ArrayList<TestpaperTestquestions>();
            testpaperTestquestionsList = testpaperTestquestionsService.list(testpaperTestquestions);
            for (int i = 0; i < testpaperTestquestionsList.size(); i++) {
                TestpaperQuestions testpaperQuestions = new TestpaperQuestions();
                testpaperQuestions = testpaperQuestionsService.getById(testpaperTestquestionsList.get(i).getTestQuestionsId());
                if (testpaperQuestions != null) {
                    tqvolist.add(testpaperQuestions);

                }
            }
        }

        ModelMap mmapcid = new ModelMap();
        String name = ShiroUtils.getLoginName();
        TestpaperQuestions tpq = null;
        for (TestpaperQuestions tpqvo : tqvolist) {
            tpq = tpqvo;
            String tpoastr = tpqvo.getTestPaperOptionAnswerArr();
            String[] tpoaArr = tpoastr.substring(0, tpoastr.length() - 1).split(";");
            if ("4".equals(tpq.gettQuestiontemplateNum())) {
                for (int i = 0; i < tpoaArr.length; i++) {
                    TestpaperOptinanswer toa = new TestpaperOptinanswer();
                    toa = testpaperOptinanswerService.getById(tpoaArr[i]);
                    CoursetestStuoptionanswer coursetestStuoptionanswer = new CoursetestStuoptionanswer();
                    coursetestStuoptionanswer.setCreateId(Integer.parseInt(studentID));
                    coursetestStuoptionanswer.setStuanswer(toa.getStanswer());
                    List<CoursetestStuoptionanswer> stuoa2 = coursetestStuoptionanswerService.list(coursetestStuoptionanswer);
                    if (stuoa2.size() != 0) {
                        CoursetestStuoptionanswer stuoa = stuoa2.get(0);
                        if (stuoa == null) {
                            stuoa = new CoursetestStuoptionanswer();
                            stuoa.setStoption(toa.getStoption());
                            stuoa.setStuanswer("F");
                            coursetestStuoptionanswerService.saveOrUpdate(stuoa);
                        }
                    }

                }
            }
        }
        for (int i = 0; i < tqvolist.size(); i++) {
            StuQuestionScore stuQuestionScore = new StuQuestionScore();
            stuQuestionScore.setTestPaperQuestion(tqvolist.get(i).getId());
            stuQuestionScore.setStuId(studentID);
            List<StuQuestionScore> stuQuestionScoreList = stuQuestionScoreService.list(stuQuestionScore);
            if (stuQuestionScoreList.size() != 0) {
                tqvolist.get(i).setStuQuestionScore(stuQuestionScoreList.get(0));
            }
            TestpaperOptinanswer testpaperOptinanswer = new TestpaperOptinanswer();
            String[] anList = tqvolist.get(i).getTestPaperOptionAnswerArr().split(";");
            String[] stuAnList = tqvolist.get(i).getTestPaperOptionAnswerArr().split(";");
            List<TestpaperOptinanswer> testpaperOptinanswerList = new ArrayList<TestpaperOptinanswer>();
            List<CoursetestStuoptionanswer> coursetestStuoptionanswerListLater = new ArrayList<CoursetestStuoptionanswer>();
            for (int j = 0; j < anList.length; j++) {
                testpaperOptinanswer = testpaperOptinanswerService.getById(anList[j]);
                if (testpaperOptinanswer != null) {
                    testpaperOptinanswerList.add(testpaperOptinanswer);
                }
            }
            List<CoursetestStuoptionanswer> coursetestStuoptionanswerList = new ArrayList<CoursetestStuoptionanswer>();
            for (int j = 0; j < stuAnList.length; j++) {
                CoursetestStuoptionanswer coursetestStuoptionanswer = new CoursetestStuoptionanswer();
                coursetestStuoptionanswer.setCreateId(Integer.parseInt(studentID));
                coursetestStuoptionanswer.setTestPaperOptionAnswer(stuAnList[j]);
                coursetestStuoptionanswerList = coursetestStuoptionanswerService.list(coursetestStuoptionanswer);
                if (coursetestStuoptionanswerList.size() != 0) {
                    coursetestStuoptionanswerListLater.add(coursetestStuoptionanswerList.get(0));
                }
            }

            tqvolist.get(i).setCoursetestStuoptionanswerList(coursetestStuoptionanswerListLater);
            tqvolist.get(i).setTestpaperOptinanswerList(testpaperOptinanswerList);
        }
        tqvolist.sort((x, y) -> Double.compare(Double.parseDouble(x.getTitleTypeNum()), Double.parseDouble(y.getTitleTypeNum())));//这方法需要jdk1.8以上
        return tqvolist;
    }



    /**
     * 改卷保存答案
     *
     * @param paperId
     * @param
     */
    @RequestMapping("/saveScore")
    @RequiresPermissions("school:onlineExam:course:view")
    public Data saveScore(String paperId, String studentId) throws Exception {
        int sumscore = 0;
        TestpaperTestquestions testpaperTestquestions = new TestpaperTestquestions();
        testpaperTestquestions.setTestPaperId(paperId);
        List<TestpaperTestquestions> listtptq = testpaperTestquestionsService.list(testpaperTestquestions);
        UserTest userTest = new UserTest();
        userTest.setUserId(studentId);
        userTest.setTestPaperId(paperId);
        userTest.setCreateId(getUser().getUserAttrId());
        UserTest userTest1 = new UserTest();
        List<UserTest> userTestList = userTestService.list(userTest);
        if (userTestList.size() != 0) {
            userTest1 = userTestList.get(0);
        }
        if (listtptq != null) {
            for (TestpaperTestquestions tptq : listtptq) {
                List<StuQuestionScore> stuQuestionScoreList = new ArrayList<StuQuestionScore>();
                StuQuestionScore stuQuestionScore = new StuQuestionScore();
                stuQuestionScore.setStuId(studentId);
                stuQuestionScore.setTestPaperQuestion(tptq.getTestQuestionsId());
                stuQuestionScoreList = stuQuestionScoreService.list(stuQuestionScore);

                if (stuQuestionScoreList.size() != 0) {
                    sumscore += Integer.parseInt(stuQuestionScoreList.get(0).getQuestionScore());
                }
            }
        }
        StuQuestionScore stuQuestionScore = new StuQuestionScore();
        userTest1.setStuScore(String.valueOf(sumscore));
        userTest1.setUpdateBy(ShiroUtils.getLoginName());
        userTest1.setUpdateId(getUser().getUserAttrId());
        userTest1.setMadeScore("1");
        return toAjax(userTestService.update(userTest1));
    }


    /**
     * 改卷的时候上传
     *
     * @param
     * @param questionid
     * @param questionscore
     * @return
     */
    @PostMapping("/saveStuScore")
    @RequiresPermissions("school:onlineExam:course:view")
    @ResponseBody
    public StuQuestionScore saveStuScore(String studentId, String questionid, String questionscore, String paperId) {
        StuQuestionScore stuQuestionScore = new StuQuestionScore();
        stuQuestionScore.setCreateId(studentId);
        stuQuestionScore.setStuId(studentId);
        stuQuestionScore.setTestPaperQuestion(questionid);
        List<StuQuestionScore> stuQuestionScoreList = stuQuestionScoreService.list(stuQuestionScore);
        if (stuQuestionScoreList.size() != 0) {
            StuQuestionScore stuQuestionScore1 = new StuQuestionScore();
            stuQuestionScore1.setUpdateBy(studentId);
            stuQuestionScore1.setQuestionScore(questionscore);
            stuQuestionScore1.setId(stuQuestionScoreList.get(0).getId());
            try {
                stuQuestionScoreService.update(stuQuestionScore1);
            } catch (Exception e) {
                System.out.println("ERROR:" + e.getCause());
                return null;
            }
        } else {
            StuQuestionScore stuQuestionScore1 = new StuQuestionScore();
            stuQuestionScore1.setCreateId(studentId);
            stuQuestionScore1.setStuId(studentId);
            stuQuestionScore1.setQuestionScore(questionscore);
            stuQuestionScore1.setTestPaperQuestion(questionid);
            try {
                stuQuestionScoreService.save(stuQuestionScore1);

            } catch (Exception e) {
                System.out.println("Error：" + e.getCause());
                return null;
            }
        }

        System.out.println("pap:"+paperId+":::"+studentId);
        int sumscore = 0;
        TestpaperTestquestions testpaperTestquestions = new TestpaperTestquestions();
        testpaperTestquestions.setTestPaperId(paperId);
        List<TestpaperTestquestions> listtptq = testpaperTestquestionsService.list(testpaperTestquestions);
        UserTest userTest = new UserTest();
        userTest.setUserId(studentId);
        userTest.setTestPaperId(paperId);
        userTest.setCreateId(getUser().getUserAttrId());
        UserTest userTest1 = new UserTest();
        List<UserTest> userTestList = userTestService.list(userTest);
        if (userTestList.size() != 0) {
            userTest1 = userTestList.get(0);
        }
        if (listtptq != null) {
            for (TestpaperTestquestions tptq : listtptq) {
                List<StuQuestionScore> stuQuestionScoreList2 = new ArrayList<StuQuestionScore>();
                StuQuestionScore stuQuestionScore2 = new StuQuestionScore();
                stuQuestionScore2.setStuId(studentId);
                stuQuestionScore2.setTestPaperQuestion(tptq.getTestQuestionsId());
                stuQuestionScoreList2 = stuQuestionScoreService.list(stuQuestionScore);

                if (stuQuestionScoreList2.size() != 0) {
                    sumscore += Integer.parseInt(stuQuestionScoreList2.get(0).getQuestionScore());
                }
            }
        }
        StuQuestionScore stuQuestionScore3 = new StuQuestionScore();
        userTest1.setStuScore(String.valueOf(sumscore));
        userTest1.setUpdateBy(ShiroUtils.getLoginName());
        userTest1.setUpdateId(getUser().getUserAttrId());
        userTest1.setMadeScore("1");
        return stuQuestionScore;
    }


}




