package cn.wstom.web.controller.school.jiaowu;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.exception.DeleteException;
import cn.wstom.jiaowu.entity.Course;
import cn.wstom.jiaowu.entity.TeacherCourse;
import cn.wstom.jiaowu.service.CourseService;
import cn.wstom.jiaowu.service.TeacherCourseService;
import cn.wstom.jiaowu.service.TeacherService;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.MyTitleType;
import cn.wstom.onlineexam.service.MyTitleTypeService;
import cn.wstom.system.service.SysUserService;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/jiaowu/teacherCourse")
public class TeacherCourseController extends BaseController {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private TeacherCourseService teacherCourseService;
    @Autowired
    private MyTitleTypeService myTitleTypeService;
    @PostMapping("/noSelect/{id}")
    @ResponseBody
    public TableDataInfo noSelect(@PathVariable("id") String id, ModelMap modelMap) {
        TeacherCourse tc = new TeacherCourse();
        tc.setTid(id);
        List<TeacherCourse> tcs = teacherCourseService.list(tc);

        startPage();
        List<Course> courses = courseService.list(new Course());

        List<Course> courseList = new ArrayList<>();
        Map<String, TeacherCourse> map = new HashMap<>();
        for (TeacherCourse t : tcs) {
            map.put(t.getCid(), t);
        }
        for (Course c : courses) {
            if (map.get(c.getId()) != null) {
                courseList.add(c);
            }
        }

        courseList.forEach(courses::remove);

        return wrapTable(courses);
    }
   /***
   *初始化教师课程题型
   */
    @GetMapping("/chushihua")
    @ResponseBody
    public String chushihua() {
        TeacherCourse tc = new TeacherCourse();
        List<TeacherCourse> tcs = teacherCourseService.list(tc);
        for ( int i=0 ; i<tcs.size();i++){
            System.out.println(tcs.get(i).getId());
            System.out.println(tcs.get(i).getCid());
            System.out.println(tcs.get(i).getTid());
            List<MyTitleType> mtys = myTitleTypeService.findByCidAndTid(tcs.get(i).getCid(),tcs.get(i).getTid());
            if(mtys.size()==0){
                MyTitleType mty =new MyTitleType();
                mty.setCid(tcs.get(i).getCid());
                mty.setCreateId(tcs.get(i).getTid());
                mty.setPublicTitleId("40288b0e54964a390154968c219e0000");
                mty.setTitleTypeName("单项选择题");
                try {
                    myTitleTypeService.save(mty);
                } catch (Exception e) {
                    System.out.println("初始化选择题失败");
                    e.printStackTrace();
                }
                MyTitleType mty1 =new MyTitleType();
                mty1.setCid(tcs.get(i).getCid());
                mty1.setCreateId(tcs.get(i).getTid());
                mty1.setPublicTitleId("8a94ab4e4d310474014d3168e29c0001");
                mty1.setTitleTypeName("判断题");
                try {
                    myTitleTypeService.save(mty1);
                } catch (Exception e) {
                    System.out.println("初始化填空题失败");
                    e.printStackTrace();
                }
                MyTitleType mty2 =new MyTitleType();
                mty2.setCid(tcs.get(i).getCid());
                mty2.setCreateId(tcs.get(i).getTid());
                mty2.setPublicTitleId("8a94be094cdccb13014cdcce42b40000");
                mty2.setTitleTypeName("多项选择题");
                try {
                    myTitleTypeService.save(mty2);
                } catch (Exception e) {
                    System.out.println("初始化多项选择题失败");
                    e.printStackTrace();
                }
            };
        }
        return "sususus";
    }
/**
 * 获取教师该课程的题目类型id
// * */
//@RequestMapping(value = "getAllSearch",produces = "application/json;charset=utf-8")
@GetMapping("/gettixingid")
@ResponseBody
public List<String> gettixingid(String cid, String tid){

    List<MyTitleType> mtys = myTitleTypeService.findByCidAndTid(cid, tid);
    List<String> re =new ArrayList<>();
    for ( int i=0 ; i<mtys.size();i++){
        re.add(mtys.get(i).getId());
        re.add(mtys.get(i).getTitleTypeName());
        System.out.println( "题型id"+mtys.get(i).getId());
        System.out.println("题型名称"+mtys.get(i).getTitleTypeName());
    }

    return  re;
}
    @PostMapping("/already/{id}")
    @ResponseBody
    public TableDataInfo already(@PathVariable("id")String id,  ModelMap modelMap) {

        TeacherCourse tc = new TeacherCourse();
        tc.setTid(id);
        startPage();
        List<TeacherCourse> tcs = teacherCourseService.list(tc);

        tcs.forEach(t -> {
            Map<String, Object> map = new HashMap<>();
            Course c = courseService.getById(t.getCid());
            map.put("name", c.getName());
            map.put("tcId", t.getId());
            t.setParams(map);
        });

        return wrapTable(tcs);
    }

    @RequiresPermissions("jiaowu:teacher:add")
    @Log(title = "教师课堂管理", actionType = ActionType.UPDATE)
    @PostMapping("/add/{id}")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data edit(@PathVariable("id") String id, String cId) throws Exception {
        TeacherCourse teacherCourse = new TeacherCourse();
        teacherCourse.setThumbnailPath("morentupian");
        teacherCourse.setTid(id);
        teacherCourse.setCid(cId);
        teacherCourse.setCreateBy(getLoginName());
        System.out.println("aaaa"+teacherCourse);
        try {
            return toAjax(teacherCourseService.save(teacherCourse));
        } catch (Exception e) {
            e.printStackTrace();
            throw new DeleteException();
        }
    }

    @RequiresPermissions("jiaowu:teacher:add")
    @Log(title = "教师课堂管理", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data remove(String tcId) throws DeleteException {
        try {
            return toAjax(teacherCourseService.removeById(tcId));
        } catch (Exception e) {
            throw new DeleteException();
        }
    }
}
