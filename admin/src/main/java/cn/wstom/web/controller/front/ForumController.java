package cn.wstom.web.controller.front;

import cn.wstom.main.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author dws
 * @date 2019/03/08
 */
@Controller
@RequestMapping("/forum")
public class ForumController extends BaseController {
    private static final String PREFIX = "/front/forum";
    private static final String TOPIC_PREFIX = "/school/teacher/forum";

    @RequestMapping(value = {"", "/index", "/index/{sort}"})
    public String index(@PathVariable(value = "sort", required = false) String sort,
                        @RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap) {
        if ("hot".equals(sort)) {
            sort = "hot";
        } else if ("recommend".equals(sort)) {
            sort = "recommend";
        } else {
            sort = null;
        }
        modelMap.addAttribute("sort", sort);
        modelMap.addAttribute("p", p);
        return PREFIX + "/index";
    }

    // TODO: 2019/4/1 后续和上面合并

    /**
     * @param sort
     * @param p
     * @param modelMap
     * @return
     */
    @RequestMapping(value = {"/course", "/course/index", "/course/index/{sort}"})
    public String index2(@PathVariable(value = "sort", required = false) String sort,
                         @RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap) {
        if ("hot".equals(sort)) {
            sort = "hot";
        } else if ("recommend".equals(sort)) {
            sort = "recommend";
        } else {
            sort = null;
        }
        modelMap.addAttribute("sort", sort);
        modelMap.addAttribute("p", p);
        return TOPIC_PREFIX + "/topicManage";
    }
}
