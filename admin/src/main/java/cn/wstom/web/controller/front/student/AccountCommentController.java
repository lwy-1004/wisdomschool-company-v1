package cn.wstom.web.controller.front.student;

import cn.wstom.common.base.Data;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.square.entity.Deck;
import cn.wstom.square.entity.Reply;
import cn.wstom.square.entity.TopicComment;
import cn.wstom.square.service.DeckService;
import cn.wstom.square.service.ReplyService;
import cn.wstom.square.service.TopicCommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liniec
 * @date 2020/01/20 18:10
 *  学生管理个人评论
 *      - reply
 *      - deck
 */
@Controller
@RequestMapping("/account/comment")
public class AccountCommentController extends BaseController {

    private final String COMMENT_TYPE_REPLY = "reply";
    private final String COMMENT_TYPE_DECK = "deck";


    @Autowired
    private ReplyService replyService;
    @Autowired
    private DeckService deckService;
    @Autowired
    private TopicCommentService commentService;

    /**
     * 获取所有回复以及二级回复
     * @return
     */
    @GetMapping("/index")
    public String indexComment(ModelMap modelMap) {

        TopicComment topicComment = new TopicComment();
        topicComment.setCreateBy(getUserId());
        startPage();
        List<TopicComment> comments = commentService.list(topicComment);

        modelMap.put("pageModel", wrapTable(comments));

        return "/front/account/comment";
    }

    @ApiOperation("删除个人评论")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    @ResponseBody
    public Data remove(@RequestParam("id") String id, @RequestParam("type") String type) throws Exception {
        if (type.equals(COMMENT_TYPE_REPLY)) {
            return toAjax(replyService.removeById(id));
        }
        if (type.equals(COMMENT_TYPE_DECK)) {
            return toAjax(deckService.removeById(id));
        }
        return Data.error();
    }
}
