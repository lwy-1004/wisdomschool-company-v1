package cn.wstom.web.controller.wechat.account;

import cn.wstom.main.web.base.BaseController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/wx/work")
@Slf4j
public class WechatWorkController extends BaseController {

    @Autowired
    private WxMpService wxMpService;

    @ApiOperation("作业首页")
    @GetMapping("/index")
    public String index(@RequestParam("openId") String openId, ModelMap modelMap) {
        return "/wx/work/index";
    }

}
