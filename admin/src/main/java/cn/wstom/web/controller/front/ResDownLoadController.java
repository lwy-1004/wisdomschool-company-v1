package cn.wstom.web.controller.front;

import cn.wstom.common.utils.AtomicIntegerUtil;
import cn.wstom.jiaowu.entity.Recourse;
import cn.wstom.jiaowu.service.RecourseService;
import cn.wstom.main.web.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *  资源下载重定向
 * @author liniec
 * @version 2019/09/30 13:18
 */
@Controller
@RequestMapping("/resource")
@Slf4j
public class ResDownLoadController extends BaseController {

    @Value("${wstom.storageContextPath}")
    private String STORAGE_URL;

    @Autowired
    private RecourseService recourseService;

    @RequestMapping("/download/{resourceId}")
    public String redirect(@PathVariable String resourceId) {
        //  下载次数的更新
        Recourse recourse = recourseService.selectByAttrId(resourceId);
        if (!AtomicIntegerUtil.isEmplyInMap(recourse.getClass(), recourse.getId()))
            AtomicIntegerUtil.getInstance(recourse.getClass(), recourse.getId(), Long.valueOf(recourse.getCount()));
        recourse.setCount(AtomicIntegerUtil.getInstance(recourse.getClass(), recourse.getId()).getAndIncrement());
        return "redirect:" + STORAGE_URL + "/downloadFile?fileId=" + resourceId;
    }
}
