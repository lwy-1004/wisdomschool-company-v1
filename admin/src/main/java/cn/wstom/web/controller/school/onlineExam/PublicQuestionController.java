package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.support.Convert;
import cn.wstom.jiaowu.service.DepartmentService;
import cn.wstom.jiaowu.service.MajorService;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.MyOptionAnswer;
import cn.wstom.onlineexam.entity.MyQuestions;
import cn.wstom.onlineexam.entity.PublicOptionAnswer;
import cn.wstom.onlineexam.entity.PublicQuestions;
import cn.wstom.onlineexam.service.MyOptionAnswerService;
import cn.wstom.onlineexam.service.MyQuestionsService;
import cn.wstom.onlineexam.service.PublicOptionAnswerService;
import cn.wstom.onlineexam.service.PublicQuestionsService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.controller.school.onlineExam.common.util.Uuid;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 题库中心
 *
 * @author hzh
 */
@Controller
@RequestMapping("/school/onlineExam/publicQuestion")
public class PublicQuestionController extends BaseController {
    private String prefix = "school/onlineExam/publicQuestion";

    @Autowired
    private PublicQuestionsService publicQuestionsService;
    @Autowired
    private MyQuestionsService myQuestionsService;
    @Autowired
    private PublicOptionAnswerService publicOptionAnswerService;
    @Autowired
    private MajorService majorService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private MyOptionAnswerService myOptionAnswerService;

    @Autowired
    private SysUserService sysUserService;
    private PublicOptionAnswer publicOptionAnswer;

    @RequiresPermissions("teacher:course:view")
    @GetMapping("/{cid}")
    public String toPublicList(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("id", cid);
        return prefix + "/list";
    }

    /**
     * 题库页面
     *
     * @param publicQuestions
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list/{cid}")
    @ResponseBody
    public TableDataInfo list(@PathVariable String cid, PublicQuestions publicQuestions) {
        startPage();
        publicQuestions.setXzsubjectsId(cid);
        List<PublicQuestions> list = publicQuestionsService.list(publicQuestions);
        String easy = "容易";
        String easyNum = "1";
        String relativelyEasy = "较易";
        String relativelyEasyNum = "2";
        String secondary = "中等";
        String secondaryNum = "3";
        String moreDifficult = "较难";
        String moreDifficultNum = "4";
        String difficulty = "困难";
        String difficultyNum = "5";
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getDifficulty().equals(easyNum)) {
                    list.get(i).setDifficulty(easy);
                }
                if (list.get(i).getDifficulty().equals(relativelyEasyNum)) {
                    list.get(i).setDifficulty(relativelyEasy);
                }
                if (list.get(i).getDifficulty().equals(secondaryNum)) {
                    list.get(i).setDifficulty(secondary);
                }
                if (list.get(i).getDifficulty().equals(moreDifficultNum)) {
                    list.get(i).setDifficulty(moreDifficult);
                }
                if (list.get(i).getDifficulty().equals(difficultyNum)) {
                    list.get(i).setDifficulty(difficulty);
                }
            }
        }
        return wrapTable(list);
    }


    /**
     * 添加
     */
    @Log(title = "新增题目", actionType = ActionType.INSERT)
    @RequiresPermissions("teacher:course:view")
    @PostMapping("/save")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addSave(PublicQuestions publicQuestions) throws Exception {
        String[] optionanswersArr = null;
        String[] optionanswerArr = null;
        String optionAnswerIDList = "";
        String trimstr = "option=";
        String myoaStr = "";
        String optionanswers = publicQuestions.getPublicoptionAnswerArr();


        List<MyOptionAnswer> myoalist = new ArrayList<MyOptionAnswer>();
        optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");//optionanswersArr按;进行分割，这里分割成4个

        for (int i = 0; i < optionanswersArr.length; i++) {
            optionanswerArr = optionanswersArr[i].substring(0, optionanswersArr[i].length() - 1).split(":");//按：进行分割
            String optionone = "";
            if (optionanswerArr[0].contains(trimstr)) {
                optionone = optionanswerArr[0].substring(7);// 去掉option=开头字符串
            } else {
                optionone = optionanswerArr[0];
            }
            String answerone = optionanswerArr[1];

            try {
                String optionstr = java.net.URLDecoder.decode(optionone, "UTF-8");
                publicOptionAnswer = new PublicOptionAnswer();
                Uuid answerId = new Uuid();
                publicOptionAnswer.setCreateBy(ShiroUtils.getLoginName());
                publicOptionAnswer.setCreateId(ShiroUtils.getUserId());
                publicOptionAnswer.setId(answerId.UId());
                publicOptionAnswer.setStoption(optionstr);
                publicOptionAnswer.setStanswer(answerone);
                toAjax(publicOptionAnswerService.save(publicOptionAnswer));
                myoaStr += publicOptionAnswer.getId() + ";";

            } catch (Exception e) {
                System.out.println("乱码解决错误：" + e);
            }
        }
        publicQuestions.setCreateBy(ShiroUtils.getLoginName());
        publicQuestions.setCreateId(ShiroUtils.getUserId());
        publicQuestions.setQexposed("0");
        publicQuestions.setQmaxexposure("10000");
        publicQuestions.setStstatus(1);
        publicQuestions.setPublicoptionAnswerArr(myoaStr);
        return toAjax(publicQuestionsService.save(publicQuestions));

    }


    /**
     * 跳转添加页面
     *
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/add")
    public String toAdd() {
        return prefix + "/add";
    }

    /**
     * 编辑
     *
     * @param id
     * @param mmap
     * @return
     */
    @RequiresPermissions("teacher:course:view")
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        System.out.println(publicQuestionsService.getById(id).getTitle());
        mmap.put("publicQuestion", publicQuestionsService.getById(id));
        return prefix + "/edit";
    }


    @RequiresPermissions("teacher:course:view")
    @Log(title = "删除试题", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {

        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            for (int i = 0; i < idList.size(); i++) {
                String IdStr = publicQuestionsService.getById(idList.get(i)).getPublicoptionAnswerArr();
                if (IdStr != null) {
                    String[] answerIds = IdStr.split(";");
                    for (int j = 0; j < answerIds.length; j++) {
                        publicOptionAnswerService.removeById(answerIds[j]);
                    }
                }
            }
            return toAjax(publicQuestionsService.removeByIds(idList));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }


    @RequiresPermissions("teacher:course:view")
    @Log(title = "移入我的题库", actionType = ActionType.DELETE)
    @PostMapping("/toPrivate")
    @ResponseBody
    public Data toPrivate(String ids) {
        System.out.println("***********"+ids);
        MyQuestions myQuestions = new MyQuestions();
        String ansString = "";
        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            for (int i = 0; i < idList.size(); i++) {
                String IdStr = publicQuestionsService.getById(idList.get(i)).getPublicoptionAnswerArr();
                PublicQuestions publicQuestions = publicQuestionsService.getById(idList.get(i));
                String loginid = ShiroUtils.getUserId();
                SysUser sysUser = sysUserService.getById(loginid);
                String uid = sysUser.getUserAttrId();
                myQuestions.setCreateId(uid);
                myQuestions.setCreateBy(ShiroUtils.getLoginName());
                myQuestions.setDifficulty(publicQuestions.getDifficulty());
                myQuestions.setParsing(publicQuestions.getParsing());
                myQuestions.setQexposed("0");
                myQuestions.setQmaxexposure("10000");
                myQuestions.setStstatus(publicQuestions.getStstatus());
                myQuestions.setTitle(publicQuestions.getTitle());
                myQuestions.setYear(publicQuestions.getYear());
                MyQuestions myQuestions1 = new MyQuestions();
                myQuestions1.setTemplateNum(publicQuestions.getTemplateNum());
                List<MyQuestions> myQuestionsList=myQuestionsService.list(myQuestions1);
                if (myQuestionsList.size()==0){
                    return Data.error("题库没有相应的题型");
                }else{
                    for (int j = 0; j < myQuestionsList.size(); j++) {
                        myQuestions.setTitleTypeId(myQuestionsList.get(i).getTitleTypeId());
                    }

                }
                myQuestions.setXzsubjectsId(publicQuestions.getXzsubjectsId());
                myQuestions.setJchapterId(publicQuestions.getjChapterId());
                if (IdStr != null) {
                    String[] answerIds = IdStr.split(";");
                    for (int j = 0; j < answerIds.length; j++) {
                        MyOptionAnswer myOptionAnswer = new MyOptionAnswer();
                        PublicOptionAnswer publicOptionAnswer = new PublicOptionAnswer();
                        publicOptionAnswer = publicOptionAnswerService.getById(answerIds[j]);
                        Uuid uuid1 = new Uuid();
                        String newID = uuid1.UId();
                        myOptionAnswer.setId(newID);
                        myOptionAnswer.setCreateBy(ShiroUtils.getLoginName());
                        String loginid1 = ShiroUtils.getUserId();
                        SysUser sysUser1 = sysUserService.getById(loginid);
                        String uid1 = sysUser.getUserAttrId();
                        myOptionAnswer.setCreateId(uid1);
                        ansString+=newID+";";
                        if (publicOptionAnswer.getUpdateTime() != null) {
                            myOptionAnswer.setUpdateTime(publicOptionAnswer.getUpdateTime());

                        }
                        if (publicOptionAnswer.getUpdateBy() != null) {
                            myOptionAnswer.setUpdateBy(publicOptionAnswer.getUpdateBy());

                        }
                        if (publicOptionAnswer.getStanswer() != null) {
                            myOptionAnswer.setStanswer(publicOptionAnswer.getStanswer());

                        }
                        if (publicOptionAnswer.getStoption() != null) {
                            myOptionAnswer.setStoption(publicOptionAnswer.getStoption());

                        }

                        myOptionAnswerService.save(myOptionAnswer);
                    }
                }
            }
            myQuestions.setMyoptionAnswerArr(ansString);
                return toAjax(myQuestionsService.save(myQuestions));

        } catch (Exception e) {
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            return Data.error();
        }
    }

    @Log(title = "修改题目", actionType = ActionType.INSERT)
    @PostMapping("/update")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public Data addUpdate(PublicQuestions publicQuestions) throws Exception {
        String  loginid = ShiroUtils.getUserId();
        SysUser sysUser  = sysUserService.getById(loginid);
        String  uid = sysUser.getUserAttrId();
        String[] optionanswersArr = null;
        String[] optionanswerArr = null;
        String optionAnswerIDList = "";
        String trimstr = "option=";
        String myoaStr = "";
        String optionanswers = publicQuestions.getPublicoptionAnswerArrContent();
        List<PublicOptionAnswer> myoalist = new ArrayList<PublicOptionAnswer>();
        optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");//optionanswersArr按;进行分割，这里分割成4个
        myoaStr = publicQuestions.getPublicoptionAnswerArr();
        if (myoaStr != null && !myoaStr.equals("")) {
            String[] myoaArr = myoaStr.substring(0, myoaStr.length() - 1).split(";");
            for (int i = 0; i < myoaArr.length; i++) {
                System.out.println("_++_+_" + myoaArr[i]);
                myoalist.add(publicOptionAnswerService.getById(myoaArr[i]));
                System.out.println("getStanswer:" + publicOptionAnswerService.getById(myoaArr[i]).getStanswer());
            }
        }
        if (optionanswers != null && !optionanswers.equals("")) {
            optionanswersArr = optionanswers.substring(0, optionanswers.length() - 1).split(";");
            for (int i = 0; i < optionanswersArr.length; i++) {
                System.out.println("optionanswersArr[i]==="
                        + optionanswersArr[i]);
                optionanswerArr = optionanswersArr[i].substring(0,
                        optionanswersArr[i].length() - 1).split(":");
                String optionone = "";
                if (optionanswerArr[0].contains(trimstr)) {
                    optionone = optionanswerArr[0].substring(7);// 去掉option=开头字符串
                } else {
                    optionone = optionanswerArr[0];
                }
                try {
                    String optionstr = java.net.URLDecoder.decode(optionone,
                            "UTF-8");
                    String answerone = optionanswerArr[1];
                    if (myoalist.size() != 0 && !myoalist.isEmpty()) {
                        publicOptionAnswer = myoalist.get(i);
                        publicOptionAnswer.setUpdateBy(ShiroUtils.getLoginName());
                        publicOptionAnswer.setUpdateId(uid);
                        publicOptionAnswer.setStoption(optionstr);
                        publicOptionAnswer.setStanswer(answerone);
                        publicOptionAnswer.getStoption();
                        publicOptionAnswer.getStanswer();
                        this.publicOptionAnswerService.update(publicOptionAnswer);
                        myoalist.remove(i);
                    } else {
                        publicOptionAnswer = new PublicOptionAnswer();
                        Uuid answerId = new Uuid();
                        publicOptionAnswer.setUpdateBy(ShiroUtils.getLoginName());
                        publicOptionAnswer.setUpdateId(uid);
                        publicOptionAnswer.setId(answerId.UId());
                        publicOptionAnswer.setStoption(optionstr);
                        publicOptionAnswer.setStanswer(answerone);
                        this.publicOptionAnswerService.save(publicOptionAnswer);
                        myoaStr += publicOptionAnswer.getId() + ";";
                    }
                } catch (Exception e) {
                }
            }
            System.out.println("===" + optionAnswerIDList);
        }
        publicQuestions.setPublicoptionAnswerArr(myoaStr);
        publicQuestions.setCreateId(uid);
        publicQuestions.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(publicQuestionsService.update(publicQuestions));

    }

}
