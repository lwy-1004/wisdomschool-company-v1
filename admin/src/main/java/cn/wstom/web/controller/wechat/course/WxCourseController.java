package cn.wstom.web.controller.wechat.course;

import cn.wstom.common.base.Data;
import cn.wstom.common.config.Global;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.dao.pojo.WechatAccount;
import cn.wstom.dao.service.WechatAccountService;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.entity.Course;
import cn.wstom.jiaowu.entity.Recourse;
import cn.wstom.jiaowu.entity.RecourseType;
import cn.wstom.jiaowu.entity.TeacherCourse;
import cn.wstom.jiaowu.service.*;
import cn.wstom.main.async.AsyncManager;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.web.hook.async.AsyncIntegral;
import cn.wstom.wechat.supports.http.WxResult;
import cn.wstom.wechat.supports.http.WxResultContracts;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;

import java.util.List;
import java.util.Map;

/**
 * 微信公众号课程
 * @author Liniec
 * @create 2019年9月28日 01点07分
 **/
@Controller
@RequestMapping("/wx/course")
@Slf4j
public class WxCourseController extends BaseController{
    private static final String STORAGE_URL_PREFIX = Global.getConfig("wstom.file.storage-url");

    @Value("${wstom.storageContextPath}")
    private String storageContextPath;

    @Autowired
    private RestOperations restOperations;

    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private WechatAccountService wechatAccountService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private TeacherCourseService teacherCourseService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private RecourseTypeService recourseTypeService;

    @Autowired
    private RecourseService recourseService;


    @ApiOperation("课程首页")
    @GetMapping("/index")
    public String index(@RequestParam String openId, ModelMap modelMap) {
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        List<ClbumCourseVo> courses = courseService.selectCoursesByStuId(Integer.parseInt(account.getStudentId()));
        modelMap.put("openId", openId);
        modelMap.put("courseVos", courses);
        return "/wx/course/index";
    }

    /**
     *
     * @param tcid 教授课程id
     * @param modelMap
     * @return
     */
    @ApiOperation("课程内容")
    @GetMapping("/detail/{tcid}")
    public String detail(@PathVariable String tcid, ModelMap modelMap) {
        //  课程信息
        TeacherCourse teachCourse = teacherCourseService.getById(tcid);
        Course course = courseService.getById(teachCourse.getCid());

        //  资料类型列表
        RecourseType type = new RecourseType();
        type.setTcId(String.valueOf(tcid));
        List<RecourseType> types = recourseTypeService.list(type);

        //  课程章节
        List chapters = chapterService.getCourseChapterInfoTree(teachCourse.getCid());

        modelMap.put("teachCourse", teachCourse);
        modelMap.put("course", course);
        modelMap.put("types", types);
        modelMap.put("chapters", chapters);
        modelMap.put("resourceUrl", storageContextPath);
        return "/wx/course/detail";
    }

    /**
     * 获取课程资料
     * @param tcid 教授课程id
     * @param type 资源类型
     * @param modelMap
     * @return
     */
    @ApiOperation("资源类型")
    @PostMapping("/resources/{tcid}")
    @ResponseBody
    public Object resources(@PathVariable String tcid, @RequestParam String type, ModelMap modelMap) {
        RecourseType recourseType = new RecourseType();
        if (!"0".equals(type)) {
            recourseType.setId(type);
        }

        Recourse recourse = new Recourse();
        recourse.setCategory(recourseType);
        recourse.setTcId(tcid);
        startPage();
        List<Recourse> result = recourseService.list(recourse);

        return result;
    }

    @ApiOperation("资源列表")
    @GetMapping("/resource/{tcid}")
    @ResponseBody
    public Object resource(@PathVariable String tcid, @RequestParam("type") String type) {
        List<Recourse> data = recourseService.getResourcesByType(tcid, type);
        return new WxResult(WxResultContracts.SUCCESS, data);
    }

    @RequestMapping("/playVideo/{rid}")
    @ResponseBody
    public Data playVideo(@PathVariable String rid) {
        Recourse recourse = recourseService.getById(rid);
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>(1);
        params.add("fileId", recourse.getAttrId());
        Data result = restOperations.postForObject(STORAGE_URL_PREFIX + "/playVideo", params, Data.class);

        AsyncManager.async().execute(
                AsyncIntegral.recordIntegral(ShiroUtils.getUser(), "video_view",
                        "观看课程视频【" + result.get("data") + "】"));
        return result.put("pf", STORAGE_URL_PREFIX);
    }

}
