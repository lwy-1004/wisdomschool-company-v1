package cn.wstom.web.controller.wechat.account;

import cn.wstom.common.constant.UserConstants;
import cn.wstom.common.utils.DecryptionUtil;
import cn.wstom.common.utils.JSONUtils;
import cn.wstom.common.utils.KeyUtil;
import cn.wstom.common.utils.StringUtil;
import cn.wstom.dao.pojo.WechatAccount;
import cn.wstom.dao.service.WechatAccountService;
import cn.wstom.jiaowu.data.StudentVo;
import cn.wstom.jiaowu.entity.Student;
import cn.wstom.jiaowu.service.StudentService;
import cn.wstom.jiaowu.service.StudentVoService;
import cn.wstom.main.shiro.service.LoginService;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.exception.user.*;
import cn.wstom.system.data.LoginInfo;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.wechat.supports.http.WxResult;
import cn.wstom.wechat.supports.http.WxResultContracts;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 微信公众号账号
 * @author Liniec
 * @create 2019年9月27日 16点48分
 **/
@Controller
@RequestMapping("/wx/account")
@Slf4j
public class WechatAccountController extends BaseController {

    @Value("${wx.path}")
    private String wxUrl;

    @Value(("${wx.storageContextPath"))
    private String resourceUrl;

    private static final String REGISTER_URL = "/wx/account/register";

    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private WechatAccountService wechatAccountService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentVoService studentVoService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private LoginService loginService;

    @ApiOperation("账号检验")
    @GetMapping("/authorize")
    public Object authorize(@RequestParam("code") String code,
                          @RequestParam("returnUrl") String returnUrl) {
        //  获取用户openid
        try {
            WxMpOAuth2AccessToken auth2AccessToken = wxMpService.oauth2getAccessToken(code);
            WxMpUser user = wxMpService.oauth2getUserInfo(auth2AccessToken, null);
            String openId = user.getOpenId();
            if (null != wechatAccountService.selectByOpenId(openId)) {
                log.info("【重定向地址】 [redirect=[{}]]", returnUrl);
                return "redirect:" + returnUrl + "?openId=" + openId;
            } else {
                log.info("【重定向绑定地址】 [redirect=[{}]]", wxUrl + REGISTER_URL + "/" + openId);
                return "redirect:" + wxMpService.oauth2buildAuthorizationUrl(wxUrl + REGISTER_URL + "/" + openId + "?returnUrl=" + returnUrl, WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
            return new WxResult(WxResultContracts.FAILED);
        }
    }

    @ApiOperation("账号绑定")
    @GetMapping("/register/{openId}")
    public String register(@PathVariable String openId, @RequestParam("returnUrl") String returnUrl, HttpServletRequest request) {
        request.getSession().setAttribute("openId", openId);
        request.setAttribute("returnUrl", returnUrl);
        return "/wx/register";
    }

    @ApiOperation("账号绑定")
    @PostMapping(value = "/register")
    @ResponseBody
    public Object register(@RequestParam("encrypted") String encrypted, HttpServletRequest request) {

        String openid = (String) request.getSession().getAttribute("openId");
        if (null == openid) {
            return new WxResult(WxResultContracts.FAILED, "微信账号异常");
        }

        if (StringUtil.isBlank(encrypted))
            return new WxResult(WxResultContracts.FAILED, "请求失败");
        //  解密密码
        final String loginInfo = DecryptionUtil.decryption(encrypted, KeyUtil.getPrivateKey());
        final LoginInfo info = JSONUtils.readValue(loginInfo, LoginInfo.class);

        String loginName = info.getUsername();
        String password = String.valueOf(info.getPassword());


        log.info("【账号绑定请求】 openid=[{}], student=[{}]", openid, loginName);

        //  验证学生账号
        if (StringUtil.isBlank(loginName))
            return new WxResult(WxResultContracts.EMPTY_USERNAME, "账号不能为空");

        if (StringUtil.isBlank(password))
            return new WxResult(WxResultContracts.EMPTY_PASSWORD, "密码不能为空");

        SysUser user = sysUserService.selectUserByLoginName(loginName);

        if (null == user) {
            return new WxResult(WxResultContracts.INVALID_ACCOUNT, "账号不存在");
        }
        if (user.getUserType() != UserConstants.USER_STUDENT) {
            return new WxResult(WxResultContracts.INVALID_ACCOUNT, "账号无效");
        }

        SysUser sysUser = null;
        try {
            sysUser = loginService.login(loginName, password);
        }catch (UserNotExistsException e) {
            throw new UnknownAccountException(e.getMessage(), e);
        } catch (UserPasswordNotMatchException e) {
            throw new IncorrectCredentialsException(e.getMessage(), e);
        } catch (UserPasswordRetryLimitExceedException e) {
            throw new ExcessiveAttemptsException(e.getMessage(), e);
        } catch (UserBlockedException | RoleBlockedException e) {
            throw new LockedAccountException(e.getMessage(), e);
        }catch (Exception e){
        }



        Student student = studentService.getById(user.getUserAttrId());
        log.info("【学生账号id】 student=[{}]", Integer.valueOf(student.getId()));
        //  账号绑定
        WechatAccount wechatAccount = new WechatAccount();
        wechatAccount.setOpenid(openid);
        wechatAccount.setStudentId(student.getId());
        wechatAccountService.insertWechatStudent(wechatAccount);
        return new WxResult(WxResultContracts.SUCCESS, "账号绑定成功");
    }

    @ApiOperation("取消绑定")
    @PostMapping("/unbind")
    @ResponseBody
    public Object unbind(@RequestParam("openId") String openId, @RequestParam("studentId") String studentId) {
        log.info("【取消绑定】 openIf:[{}]; studentId[{}]", openId, studentId);
        wechatAccountService.checkStuIdAndDelByOpenId(openId, studentId);
        return new WxResult(WxResultContracts.SUCCESS, "取消绑定成功");
    }

    @ApiOperation("账户信息")
    @GetMapping("/info")
    public String info(@RequestParam("openId") String openId, ModelMap modelMap) {
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        StudentVo studentVo = studentVoService.getById(account.getStudentId());
        modelMap.put("info", studentVo);
        modelMap.put("openId", openId);
        return "/wx/account/info";
    }
}
