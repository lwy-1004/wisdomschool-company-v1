package cn.wstom.web.controller.monitor;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.enums.OnlineStatus;
import cn.wstom.main.shiro.session.OnlineSession;
import cn.wstom.main.shiro.session.OnlineSessionDAO;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.system.entity.SysUserOnline;
import cn.wstom.system.service.impl.SysUserOnlineServiceImpl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 在线用户监控
 *
 * @author dws
 */
@Controller
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {
    private String prefix = "monitor/online";

    @Autowired
    private SysUserOnlineServiceImpl userOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:online:view")
    @GetMapping()
    public String online() {
        return prefix + "/online";
    }

    @RequiresPermissions("monitor:online:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUserOnline sysUserOnline) {
        startPage();
        List<SysUserOnline> list = userOnlineService.selectUserOnlineList(sysUserOnline);
        return wrapTable(list);
    }

    @RequiresPermissions("monitor:online:batchForceLogout")
    @Log(title = "在线用户", actionType = ActionType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
    public Data batchForceLogout(@RequestParam("ids[]") String[] ids) throws Exception {
        for (String sessionId : ids) {
            SysUserOnline online = userOnlineService.getById(sessionId);
            if (online == null) {
                return error("用户已下线");
            }
            OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getId());
            if (onlineSession == null) {
                return error("用户已下线");
            }
            if (sessionId.equals(ShiroUtils.getSessionId())) {
                return error("当前登陆用户无法强退");
            }
            onlineSession.setStatus(OnlineStatus.OFF_LINE);
            online.setStatus(OnlineStatus.OFF_LINE);
            userOnlineService.save(online);
        }
        return success();
    }

    @RequiresPermissions("monitor:online:forceLogout")
    @Log(title = "在线用户", actionType = ActionType.FORCE)
    @PostMapping("/forceLogout")
    @ResponseBody
    public Data forceLogout(String sessionId) throws Exception {
        SysUserOnline online = userOnlineService.getById(sessionId);
        if (sessionId.equals(ShiroUtils.getSessionId())) {
            return error("当前登陆用户无法强退");
        }
        if (online == null) {
            return error("用户已下线");
        }
        OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getId());
        if (onlineSession == null) {
            return error("用户已下线");
        }
        onlineSession.setStatus(OnlineStatus.OFF_LINE);
        online.setStatus(OnlineStatus.OFF_LINE);
        userOnlineService.save(online);
        return success();
    }
}
