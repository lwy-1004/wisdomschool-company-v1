package cn.wstom.web.controller.front;

import cn.wstom.common.constant.UserConstants;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author dws
 * @date 2019/02/23
 */
@Controller
public class IndexController extends BaseController {
    private final String prefix = "/front";

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = {"/", "/index"})
    public String index(ModelMap model, HttpServletRequest request) {
        String order = ServletRequestUtils.getStringParameter(request, "order", "newest");
        int pageNum = ServletRequestUtils.getIntParameter(request, "pn", 1);

        SysUser user = sysUserService.getById(getUserId());


        model.put("order", order);
        model.put("pn", pageNum);
        model.put("stuid",getUser().getUserAttrId());
        return prefix + "/index";
    }
}
