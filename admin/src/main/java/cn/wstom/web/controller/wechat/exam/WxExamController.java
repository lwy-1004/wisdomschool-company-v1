package cn.wstom.web.controller.wechat.exam;

import cn.wstom.dao.pojo.WechatAccount;
import cn.wstom.dao.service.WechatAccountService;
import cn.wstom.jiaowu.data.ClbumCourseVo;
import cn.wstom.jiaowu.entity.*;
import cn.wstom.jiaowu.service.*;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.onlineexam.entity.*;
import cn.wstom.onlineexam.service.*;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import cn.wstom.web.controller.school.onlineExam.MyQuestionController;
import cn.wstom.web.controller.school.onlineExam.common.util.Uuid;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import cn.wstom.common.base.Data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 我的考试
 * @author hehaheiya
 * @create 2019/10/6 12点12分
 */

@Controller
@RequestMapping("/wx/exam")
@Slf4j
public class WxExamController extends BaseController{
    @Autowired
    private TestPaperService testPaperService;

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TestpaperOptinanswerService testpaperOptinanswerService;
    @Autowired
    private TestpaperTestquestionsService testpaperTestquestionsService;
    @Autowired
    private TestpaperQuestionsService testpaperQuestionsService;
    @Autowired
    private UserTestService userTestService;
    @Autowired
    private CoursetestStuoptionanswerService coursetestStuoptionanswerService;
    @Autowired
    private CoursepaperCoursequestionsService coursepaperCoursequestionsService;
    @Autowired
    private WechatAccountService wechatAccountService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ClbumCourseService clbumCourseService;
    @Autowired
    private TeacherCourseService teacherCourseService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private MyQuestionsService myQuestionsService;
    @Autowired
    private MyOptionAnswerService myOptionAnswerService;
    @Autowired
    private ShuatiHistoryService shuatiHistoryService;

    @GetMapping("/index")
    public String index(@RequestParam String openId,ModelMap modelMap){
            WechatAccount account = wechatAccountService.selectByOpenId(openId);
            modelMap.put("openId", openId);
        return "wx/exam/index";
    }
    /**
     * 课前作业列表
     *
     * @return
     */
    @GetMapping("/toFrontExam")
    public String toFrontExam(String openId,ModelMap modelMap){
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        UserTest userTest = new UserTest();
        userTest.setUserId(account.getStudentId().toString());
        userTest.setSumbitState("0");//    只显示未提交的试卷
        userTest.setType("3");
        List<UserTest> frontlist = userTestService.list(userTest);
        modelMap.put("frontexamlist", frontlist);
        modelMap.put("openId", openId);
        return "wx/exam/frontExam/index";
    }


    /**
     * 页面跳转
     *
     * @return
     */
    @GetMapping("/stuToTest")
    public String stuToTest(String testPaperId,String studentId,String tutId,String openId, ModelMap modelMap) {
        System.out.println("????????????????????????"+openId);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysuser =sysUserService.selectUserByUserAttrId(account.getStudentId().toString());
        TestPaper testPaper = new TestPaper();
        testPaper = testPaperService.getById(testPaperId);
        modelMap.put("testPaper", testPaper);
        modelMap.put("paperId", testPaperId);
        modelMap.put("studentId", studentId);
        modelMap.put("tutId", tutId);
        modelMap.put("openId", openId);
        modelMap.put("stuName", sysuser.getLoginName());
        modelMap.put("name", sysuser.getUserName());
        return "/wx/exam/wxstuTest";

    }

    /**
     * 开始测试 初始化考试页面
     *
     * @return
     */
    @RequestMapping("/startCoursePaper")
    @ResponseBody
    public List<TestpaperQuestions> testStart(String paperId, String studentId, String paperRule, String chapterName, String openId, ModelMap mmap) throws Exception {
       System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+openId);
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysuser =sysUserService.selectUserByUserAttrId(account.getStudentId().toString());
        System.out.println("type:"+paperRule);
        List<TestpaperQuestions> tqvolist = new ArrayList<TestpaperQuestions>();
        if (paperRule.equals("1")) {
            System.out.println("课程考试");
            CoursepaperCoursequestions coursepaperCoursequestions = new CoursepaperCoursequestions();
            coursepaperCoursequestions.setTestPaperId(paperId);
            coursepaperCoursequestions.setStudentId(studentId);
            List<CoursepaperCoursequestions> coursepaperCoursequestionsList = new ArrayList<CoursepaperCoursequestions>();
            coursepaperCoursequestionsList = coursepaperCoursequestionsService.list(coursepaperCoursequestions);
            for (int i = 0; i < coursepaperCoursequestionsList.size(); i++) {
                System.out.println("题目数量："+coursepaperCoursequestionsList.size());
                TestpaperQuestions testpaperQuestions = new TestpaperQuestions();
                testpaperQuestions = testpaperQuestionsService.getById(coursepaperCoursequestionsList.get(i).getTestQuestionsId());
                if (testpaperQuestions != null) {
                    System.out.println("加入");
                    tqvolist.add(testpaperQuestions);
                }
            }
        } else {
            TestpaperTestquestions testpaperTestquestions = new TestpaperTestquestions();
            testpaperTestquestions.setTestPaperId(paperId);
            List<TestpaperTestquestions> testpaperTestquestionsList = new ArrayList<TestpaperTestquestions>();
            testpaperTestquestionsList = testpaperTestquestionsService.list(testpaperTestquestions);
            for (int i = 0; i < testpaperTestquestionsList.size(); i++) {
                TestpaperQuestions testpaperQuestions = new TestpaperQuestions();
                testpaperQuestions = testpaperQuestionsService.getById(testpaperTestquestionsList.get(i).getTestQuestionsId());
                if (testpaperQuestions != null) {
                    tqvolist.add(testpaperQuestions);
                }
            }
        }

        ModelMap mmapcid = new ModelMap();
        String name = sysuser.getLoginName();
        TestpaperQuestions tpq = null;
        for (TestpaperQuestions tpqvo : tqvolist) {
            tpq = tpqvo;
            String tpoastr = tpqvo.getTestPaperOptionAnswerArr();
            String[] tpoaArr = tpoastr.substring(0, tpoastr.length() - 1).split(";");
            if ("4".equals(tpq.gettQuestiontemplateNum())) {
                for (int i = 0; i < tpoaArr.length; i++) {
                    TestpaperOptinanswer toa = new TestpaperOptinanswer();
                    toa = testpaperOptinanswerService.getById(tpoaArr[i]);
                    CoursetestStuoptionanswer coursetestStuoptionanswer = new CoursetestStuoptionanswer();
                    String loginid = ShiroUtils.getUserId();
                    SysUser sysUser = sysUserService.getById(loginid);
                    String uid = sysUser.getUserAttrId();
                    int newId = Integer.parseInt(uid);
                    coursetestStuoptionanswer.setCreateId(Integer.parseInt(studentId));
                    coursetestStuoptionanswer.setStuanswer(toa.getStanswer());
                    List<CoursetestStuoptionanswer> stuoa2 = coursetestStuoptionanswerService.list(coursetestStuoptionanswer);
                    if (stuoa2.size() != 0) {
                        CoursetestStuoptionanswer stuoa = stuoa2.get(0);
                        if (stuoa == null) {
                            stuoa = new CoursetestStuoptionanswer();
                            stuoa.setStoption(toa.getStoption());
                            stuoa.setStuanswer("F");
                            coursetestStuoptionanswerService.saveOrUpdate(stuoa);
                        }
                    }

                }
            }
        }
        for (int i = 0; i < tqvolist.size(); i++) {
            TestpaperOptinanswer testpaperOptinanswer = new TestpaperOptinanswer();
            String[] anList = tqvolist.get(i).getTestPaperOptionAnswerArr().split(";");
            String[] stuAnList = tqvolist.get(i).getTestPaperOptionAnswerArr().split(";");
            List<TestpaperOptinanswer> testpaperOptinanswerList = new ArrayList<TestpaperOptinanswer>();
            List<CoursetestStuoptionanswer> coursetestStuoptionanswerListLater = new ArrayList<CoursetestStuoptionanswer>();
            for (int j = 0; j < anList.length; j++) {
                testpaperOptinanswer = testpaperOptinanswerService.getById(anList[j]);
                if (testpaperOptinanswer != null) {
                    testpaperOptinanswerList.add(testpaperOptinanswer);
                }
            }
            List<CoursetestStuoptionanswer> coursetestStuoptionanswerList = new ArrayList<CoursetestStuoptionanswer>();
            for (int j = 0; j < stuAnList.length; j++) {
                CoursetestStuoptionanswer coursetestStuoptionanswer = new CoursetestStuoptionanswer();
//                String loginid = ShiroUtils.getUserId();
                String loginid = sysuser.getId();
                SysUser sysUser = sysUserService.getById(loginid);
                String uid = sysUser.getUserAttrId();
                int newId = Integer.parseInt(uid);
                coursetestStuoptionanswer.setCreateId(Integer.parseInt(studentId));
                coursetestStuoptionanswer.setTestPaperOptionAnswer(stuAnList[j]);
                coursetestStuoptionanswerList = coursetestStuoptionanswerService.list(coursetestStuoptionanswer);
                if (coursetestStuoptionanswerList.size() != 0) {
                    coursetestStuoptionanswerListLater.add(coursetestStuoptionanswerList.get(0));
                }
            }
            tqvolist.get(i).setCoursetestStuoptionanswerList(coursetestStuoptionanswerListLater);
            tqvolist.get(i).setTestpaperOptinanswerList(testpaperOptinanswerList);
        }
        tqvolist.sort((x, y) -> Double.compare(Double.parseDouble(x.getTitleTypeNum()), Double.parseDouble(y.getTitleTypeNum())));//这方法需要jdk1.8以上
        return tqvolist;
    }


    /**
     * 设置提交的状态
     */
    @RequestMapping("/setState")
    @ResponseBody
    public Data setState(@RequestParam String paperId,
                         @RequestParam String studentId, @RequestParam String tutId, @RequestParam String openId)throws Exception {
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        SysUser sysuser =sysUserService.selectUserByUserAttrId(account.getStudentId().toString());
        System.out.println("====================设置提交状态paperId:" +paperId+"--studentId--"+studentId+"--tutId--"+tutId);
        UserTest userTest1 = new UserTest();
        List<UserTest> userTestList;
        userTestList = userTestService.list(userTest1);
        userTest1.setId(tutId);
        userTest1.setSumbitState("1");
        userTest1.setUpdateBy(sysuser.getLoginName());
        userTest1.setUserId(sysuser.getUserAttrId());
        userTest1.setUpdateBy(sysuser.getLoginName());
        return toAjax( userTestService.update(userTest1));
    }

    /**
     * 上传答案
     *
     * @param questionNum
     * @param tpquestionId
     * @param anwserId
     * @param anwserStr
     */
    @PostMapping("/saveCourseTestAnswer")
    @ResponseBody
    public String SaveOrUpdateStuAnwser(@RequestParam String questionNum, @RequestParam String tpquestionId,
                                        @RequestParam String anwserId, @RequestParam String anwserStr, @RequestParam String studentId, @RequestParam String openId) {
        try {


            WechatAccount account = wechatAccountService.selectByOpenId(openId);
            SysUser sysuser =sysUserService.selectUserByUserAttrId(account.getStudentId().toString());
            System.out.println("答案："+anwserStr);
            TestpaperQuestions tpquestion = new TestpaperQuestions();
            boolean istf = true;
            String loginid = sysuser.getId();
            SysUser sysUser = sysUserService.getById(loginid);
            String uid = sysUser.getUserAttrId();
            Student stuinfo = new Student();
            stuinfo = studentService.getById(1);
//                if (stuinfo != null && !"".equals(stuinfo)) {
            tpquestion = testpaperQuestionsService.getById(tpquestionId);
            String tpoastr = tpquestion.getTestPaperOptionAnswerArr();
            String[] tpoaArr = tpoastr.substring(0, tpoastr.length() - 1).split(";");
            String[] mytpoaArr = anwserStr.substring(0, anwserStr.length() - 1).split("ks;");
            for (int i = 0; i < tpoaArr.length; i++) {
                TestpaperOptinanswer tpoa = testpaperOptinanswerService.getById(tpoaArr[i]);
                List<CoursetestStuoptionanswer> stuoaList = new ArrayList<CoursetestStuoptionanswer>();
                CoursetestStuoptionanswer stuoa = new CoursetestStuoptionanswer();
                stuoa.setCreateId(Integer.parseInt(studentId));
                stuoa.setTestPaperOptionAnswer(tpoa.getId());
                stuoaList = coursetestStuoptionanswerService.list(stuoa);
                if (stuoaList.size() == 0) {
                    stuoa = new CoursetestStuoptionanswer();
                    stuoa.setTestPaperOptionAnswer(tpoa.getId());
                    stuoa.setCreateId(Integer.parseInt(studentId));
                    stuoa.setCreateBy(sysuser.getLoginName());
                    stuoa.setStuanswer(mytpoaArr[i]);
                    stuoa.setCreateId(Integer.parseInt(studentId));
                    Uuid uid2 = new Uuid();
                    stuoa.setId(uid2.UId());
                    System.out.println("&&&&&&&&&&&&&&&&&&&"+stuoa.getStuanswer());
                    coursetestStuoptionanswerService.save(stuoa);
                } else {
                    stuoa.setUpdateId(Integer.parseInt(uid));
                    stuoa.setUpdateBy(sysuser.getLoginName());
                    stuoa.setTestPaperOptionAnswer(tpoa.getId());
                    stuoa.setStuanswer(mytpoaArr[i]);
                    stuoa.setCreateId(stuoaList.get(0).getCreateId());
                    stuoa.setTestPaperOptionAnswer(tpoaArr[i]);
                    System.out.println(" stuoa:"+ stuoa.getStuanswer());
                    coursetestStuoptionanswerService.updateByIdAns(stuoa);
                }
                TestpaperOptinanswer tpoa2 = new TestpaperOptinanswer();
                String testPaperQuestionAnsw = stuoa.getTestPaperOptionAnswer();
                tpoa2.setId(testPaperQuestionAnsw);
                tpoa2 = testpaperOptinanswerService.getById(testPaperQuestionAnsw);
                String typenum = tpquestion.gettQuestiontemplateNum();
                String str2 = stuoa.getStuanswer();
                String str1 = "";
                if (!"3".equals(typenum) && !"5".equals(typenum)) {
                    str1 = tpoa2.getStanswer();
                } else {
                    str1 = tpoa2.getStoption();
                }
                if (str1 != null) {
                    if (!str1.equals(str2)) {
                        istf = false;
                    }
                }
            }

//                } else {
//                    System.out.println("没有找到学生");
//                }
        } catch (Exception e) {
            System.out.println(e.getCause());
            return e.getMessage();

        }
        return "End";
    }


    @ApiOperation("课程列表")
    @GetMapping("/courseList")
    public String courseList(String openId,ModelMap modelMap) {
        WechatAccount account = wechatAccountService.selectByOpenId(openId);
        Student student = studentService.getById(Integer.parseInt(account.getStudentId()));
        ClbumCourse cc = new ClbumCourse();
        cc.setCid(student.getCid());
        startPage();
        List<ClbumCourse> list = clbumCourseService.list(cc);
        PageInfo<ClbumCourse> pageInfo = new PageInfo(list);
        //填充教师信息
        List<ClbumCourseVo> clbumCourseVos = new LinkedList<>();
        fillTeacherCourse(list, clbumCourseVos);
        modelMap.put("openId", openId);
        modelMap.put("clbumCourseVos",clbumCourseVos);
        return "/wx/exam/shuati/courseList";
    }

    private void fillTeacherCourse(List<ClbumCourse> list, List<ClbumCourseVo> clbumCourseVos) {
        if (list == null || list.isEmpty()) {
            return;
        }
        List<String> tcId = new LinkedList<>();
        list.forEach(l -> tcId.add(l.getTcid()));
        // 根据课程id查找教师信息
        Map<String, TeacherCourse> teacherCourseMap = teacherCourseService.mapByIds(tcId);
        tcId.clear();
        teacherCourseMap.forEach((k, v) -> tcId.add(v.getTid()));

        Map<String, SysUser> sysUserMap = userService.listByTids(tcId);

        List<String> cid = new LinkedList<>();
        list.forEach(l -> cid.add(l.getCid()));

        Map<String, Course> courseMap = courseService.mapByClbumId(cid);

        List<String> tid = new LinkedList<>();
        teacherCourseMap.forEach((k, v) -> tid.add(v.getTid()));
        Map<String, Teacher> teachers = teacherService.mapByIds(tid);

        list.forEach(l -> {
            ClbumCourseVo cc = new ClbumCourseVo();
            cc.setTeacher(teachers.get(teacherCourseMap.get(l.getTcid()).getTid()));
            TeacherCourse teacherCourse = teacherCourseMap.get(l.getTcid());
            cc.setTeacherCourse(teacherCourse);
            cc.setCourse(courseMap.get(teacherCourse.getCid()));
            cc.setSysUser(sysUserMap.get(teacherCourse.getTid()));
            clbumCourseVos.add(cc);
        });
    }

    @ApiOperation("章节列表")
    @GetMapping("/chapterList")
    public String chapterList(String openId,String cid,ModelMap modelMap) {
        Course course = courseService.getById(cid);
        Chapter chapter = new Chapter();
        chapter.setCid(course.getId());
        chapter.setPid("0");
        List<Chapter> chapterList = chapterService.list(chapter);
        modelMap.put("chapterList", chapterList);
        modelMap.put("openId", openId);
        return "/wx/exam/shuati/chapterList";
    }

    @ApiOperation("章节刷题")
    @GetMapping("/chapterShuati")
    public String chapterShuati(String openId,String chid,ModelMap modelMap) {
        String myQuestionsIds="";
        System.out.println("!!!!!!!!!!!!!!!!!!"+chid);
        MyQuestions myQuestions=new MyQuestions();
        myQuestions.setChapterId(chid);
        List<MyQuestions> list=myQuestionsService.selectList(myQuestions);
        Chapter chapter=new Chapter();
        chapter.setPid(chid);
        List<Chapter> chapters=chapterService.selectList(chapter);
        for(Chapter s : chapters){
            myQuestions.setChapterId(s.getId());
            list.addAll(myQuestionsService.selectList(myQuestions));
        }
        for (MyQuestions q : list){
            System.out.println("??????????"+q);
        }
        if(list.size()==0){
            System.out.println("此章节没有答案");
            return null;
        }
        List<Shuati> shuatis=new ArrayList<Shuati>();
        for (MyQuestions q : list){
            myQuestionsIds=myQuestionsIds+q.getId()+",";
            Shuati shuati=new Shuati();
            shuati.setMyQuestions(q);
            shuatis.add(shuati);
        }
        List<MyOptionAnswer[]> myOptionAnswers=new ArrayList<MyOptionAnswer[]>();
        for(int i=0;i<list.size();i++){
            String myoptionAnswerArrString=list.get(i).getMyoptionAnswerArr();
            String[] myoptionAnswer=myoptionAnswerArrString.split(";");
            List<MyOptionAnswer> myOptionAnswersArr=new ArrayList<MyOptionAnswer>();
            for(int j=0;j<myoptionAnswer.length;j++){
                MyOptionAnswer myOptionAnswer= myOptionAnswerService.getById(myoptionAnswer[j]);
                myOptionAnswersArr.add(myOptionAnswer);
            }
            shuatis.get(i).setMyOptionAnswerList(myOptionAnswersArr);
        }
        modelMap.put("myQuestionsIds",myQuestionsIds);
        modelMap.put("shuatis",shuatis);
        modelMap.put("listsize",shuatis.size());
        for(Shuati s: shuatis){
            System.out.println(s.toString());
        }
        return "/wx/exam/shuati/chapterShuati";
    }

    /**
     * 提交刷题
     */
    @RequestMapping("/submitShuati")
    private @ResponseBody Integer submitShuati(@RequestBody List<ShuatiHistory> shuatiHistories){
        for(ShuatiHistory s : shuatiHistories){
            s.setUserId("1632");
        }
        System.out.println(shuatiHistories.toString());
        try{
            shuatiHistoryService.saveBatch(shuatiHistories);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 1;
    }
}
