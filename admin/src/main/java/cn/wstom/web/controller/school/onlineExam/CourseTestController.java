
package cn.wstom.web.controller.school.onlineExam;

import cn.wstom.common.annotation.Log;
import cn.wstom.common.base.Data;
import cn.wstom.common.enums.ActionType;
import cn.wstom.common.support.Convert;
import cn.wstom.main.util.ShiroUtils;
import cn.wstom.main.web.base.BaseController;
import cn.wstom.main.web.page.TableDataInfo;
import cn.wstom.onlineexam.entity.TestPaper;
import cn.wstom.onlineexam.service.TestPaperService;
import cn.wstom.onlineexam.service.UserTestService;
import cn.wstom.system.entity.SysUser;
import cn.wstom.system.service.SysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * 试卷 信息操作处理stuToTest
 *
 * @author hzh
 * @date 20190223
 */

@Controller
@RequestMapping("/school/onlineExam/courseTest")
public class CourseTestController extends BaseController {
    private String prefix = "school/onlineExam/courseTest";

    @Autowired
    private UserTestService userTestService;

    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private SysUserService sysUserService;

    @RequiresPermissions("teacher:course:view")
    @GetMapping("{cid}")
    public String toList(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("cid", cid);
        return prefix + "/list";
    }


    /**
     * 查询试卷列表
     */

    @RequiresPermissions("teacher:course:view")
    @PostMapping("/list/{cid}")
    @ResponseBody
    public TableDataInfo list(@PathVariable String cid, TestPaper testPaper) {
        startPage();
        String logid = ShiroUtils.getUserId();
        SysUser sysUser = new SysUser();
        sysUser = sysUserService.getById(logid);
        String uid = sysUser.getUserAttrId();
        testPaper.setTsId(getUser().getUserAttrId());
        testPaper.setCoursrId(cid);
        testPaper.setType("1");

        //List<TestPaper> list=testPaperService.list(testPaper);
        List<TestPaper> list =testPaperService.findStuPaper(testPaper);
        return wrapTable(list);
    }


    /**
     * 新增试卷
     */

    @GetMapping("/add/{cid}")
    public String toAdd(@PathVariable String cid, ModelMap modelMap) {
        modelMap.put("id", cid);
        return prefix + "/add";
    }


    /**
     * 新增保存试卷
     */

    @RequiresPermissions("teacher:course:view")
    @Log(title = "试卷", actionType = ActionType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Data add(TestPaper testPaper) throws Exception {
        System.out.println(testPaper.getCoursrId());
        testPaper.setCreateBy(ShiroUtils.getLoginName());
        testPaper.setState("1");
        return toAjax(testPaperService.save(testPaper));
    }


    /**
     * 修改试卷
     */

    @GetMapping("/edit/{id}")
    @RequiresPermissions("teacher:course:view")

    public String toEdit(@PathVariable("id") String id, ModelMap mmap) {
        TestPaper testPaper = testPaperService.getById(id);
        mmap.put("testPaper", testPaper);
        return prefix + "/edit";
    }

    /**
     * 修改保存试卷
     */

    @RequiresPermissions("teacher:course:view")
    @Log(title = "试卷", actionType = ActionType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Data edit(TestPaper testPaper) throws Exception {
        return toAjax(testPaperService.update(testPaper));
    }

    /**
     * 删除试卷
     */
    @RequiresPermissions("teacher:course:view")
    @Log(title = "试卷", actionType = ActionType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Data remove(String ids) throws Exception {
        try {
            List<String> idList = Arrays.asList(Convert.toStrArray(ids));
            return toAjax(testPaperService.removeByIds(idList));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

}




