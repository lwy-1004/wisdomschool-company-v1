<#include "/common/ui.ftl"/>
<@shiro.hasRole name="admin">
    <@adminLayout  "智慧教育云平台-以科技助力教育" "wstom-首页" "wstom-社区" "分享让世界更美好">
        <div class="page-content">
            <#--<div class="page-header">
                <h1>
                    首页
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        我的课程
                    </small>
                </h1>
            </div>--><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12" id="main-page-container">
                    <div class="container-div layout-container-div clearfix">
                        <div class="row">
                            <div class="pagerContent pic-list">
                                <div class="pagerMainContent" style="display: none;">
                                    <a key="course.id" class="pic-list-constant-item" onclick="clickLoad(event)">
                                        <div class="">
                                            <img src="${storage}/showCondensedPicture?fileId=${teacherCourse.thumbnailPath}" key="thumbnailPath" class="pic-list-constant-img">
                                            <div class="cover-info">
                                                <i class="fa fa-list-ul"></i> &nbsp;<h4 key="course.name"></h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="m-content">
                                </div>
                            </div>
                        </div>
                    </div><!--/.container-div-->
                </div>
            </div><!-- /.row -->

        </div><!-- /.page-content -->
    </@adminLayout>
</@shiro.hasRole>

<@shiro.hasRole name="teacher">
    <@adminLayout  "智慧教育云平台-以科技助力教育" "wstom-首页" "wstom-社区" "分享让世界更美好">
        <div class="page-content">
            <#--<div class="page-header">
                <h1>
                    首页
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        我的课程
                    </small>
                </h1>
            </div>&ndash;&gt;<!-- /.page-header -->
            <div class="row">
                <div class="col-xs-12" id="main-page-container">
                    <div class="container-div layout-container-div clearfix">
                        <div class="row">
                            <div class="pagerContent pic-list">
                                <div class="pagerMainContent" style="display: none;">
                                    <a key="course.id" class="pic-list-constant-item" onclick="TclickLoad(event)">
                                        <div class="">
                                            <img src="${storage}/showCondensedPicture?fileId=${teacherCourse.thumbnailPath}" key="thumbnailPath" class="pic-list-constant-img">
                                            <div class="cover-info">
                                                <i class="fa fa-list-ul"></i> &nbsp;<h4 key="course.name"></h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="m-content">
                                </div>
                            </div>
                        </div>
                    </div><!--/.container-div-->
                </div>
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </@adminLayout>
</@shiro.hasRole>

<#--<@shiro.lacksRole name="teacher">-->
<#--<@shiro.hasRole name="sds_admin">-->
<#--    <@adminLayout  "智慧教育云平台-以科技助力教育" "wstom-首页" "wstom-社区" "分享让世界更美好">-->
<#--        <div class="page-content">-->
<#--            &lt;#&ndash;<div class="page-header">-->
<#--                <h1>-->
<#--                    首页-->
<#--                    <small>-->
<#--                        <i class="ace-icon fa fa-angle-double-right"></i>-->
<#--                        我的课程-->
<#--                    </small>-->
<#--                </h1>-->
<#--            </div>&ndash;&gt;<!-- /.page-header &ndash;&gt;-->

<#--            <div class="row">-->
<#--                <div class="col-xs-12" id="main-page-container">-->
<#--                    <div class="container-div layout-container-div clearfix">-->
<#--                        <div class="row">-->
<#--                            <div class="pagerContent pic-list">-->
<#--                                <div class="pagerMainContent" style="display: none;">-->
<#--                                    <a key="gdid"  class="pic-list-constant-item" onclick="TclickLoad(event)">-->
<#--                                        <div class="">-->
<#--                                            <div class="cover-info">-->
<#--                                                <i class="fa fa-list-ul"></i> &nbsp;<h4 key="gradess.name"></h4>-->
<#--                                                <div>-->
<#--                                                    <h4 key="departments.name"></h4>-->
<#--                                                </div>-->
<#--                                            </div>-->
<#--                                        </div>-->
<#--                                    </a>-->
<#--                                </div>-->
<#--                                <div id="m-content">-->
<#--                                </div>-->
<#--                            </div>-->
<#--                        </div>-->
<#--                    </div><!--/.container-div&ndash;&gt;-->
<#--                </div>-->
<#--            </div><!-- /.row &ndash;&gt;-->
<#--        </div><!-- /.page-content &ndash;&gt;-->
<#--    </@adminLayout>-->
<#--</@shiro.hasRole>-->
<#--</@shiro.lacksRole>-->