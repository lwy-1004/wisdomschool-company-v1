<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/js/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/browser.min.js"></script>
    <script type="/js/text/babel"></script>
    <script type="text/javascript">
        // 获取谷歌版本
        function getChromeVersion() {
            var arr = navigator.userAgent.split(' ');
            var chromeVersion = '';
            for(var i=0;i < arr.length;i++){
                if(/chrome/i.test(arr[i]))
                    chromeVersion = arr[i]
            }
            if(chromeVersion){
                return Number(chromeVersion.split('/')[1].split('.')[0]);
            } else {
                return false;
            }
        }
        if(getChromeVersion()) {
            var version = getChromeVersion();
            if(version < 55) {
                window.location.href = "${ctx}/remind";
            }
        }


        function IEVersion(){
            var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
            var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1;
            if(isIE) {
                var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
                reIE.test(userAgent);
                var fIEVersion = parseFloat(RegExp["$1"]);
                if (fIEVersion == 7) {
                    window.location.href = "${ctx}/remind";
                } else if (fIEVersion == 8) {
                    window.location.href = "${ctx}/remind";
                } else if (fIEVersion == 9) {
                    window.location.href = "${ctx}/remind";
                } else if (fIEVersion == 10) {
                    window.location.href = "${ctx}/remind";
                } else {
                    window.location.href = "${ctx}/remind";//IE版本<=7
                }
                }
        }


        if (!!window.ActiveXObject || "ActiveXObject" in window) {
            window.location.href = "${ctx}/remind";
        }

        window.onload = function () {

            //application/vnd.chromium.remoting-viewer 可能为360特有
            var is360 = _mime("type", "application/vnd.chromium.remoting-viewer");

            if (isChrome() && is360) {
                window.location.href = "${ctx}/remind";
            }
        }

    </script>

    <style>
        .text-center {
            text-align: center;
        }

        .forget-label{
            left : 15px;
            font-size: 15px;
        }

        .forget-input{
            width : 60%;
        }

        .no-margin{
            margin-left: 0 !important;
            margin-right: 0 !important;
        }


    </style>

</head>
<body>
<script>
    let wp = window;
    if(wp.parent&&wp.parent!==wp) {
        while(wp.parent&&wp.parent!==wp) {
            wp=wp.parent;
        }
        wp.location.href='${ctx}/login';
    }
</script>
<div class="login-container">
    <div class="page-top-box">
        <div class="pagemm top">
            <h1 class="navbar-text" style="font-size: 35px;position:absolute;text-align: center;margin: 3px; margin-left: 50px;color: #e0eee8;">智慧教育云</h1>
            <h3 class="navbar-text" style="font-size: 25px;position:absolute;margin-left: 260px; text-align: center;color: #e0eee8;font-family: cursive;">教师独立的SPOC教学服务平台
            </h3>
        </div>
    </div>

            <div class="login-content " style="overflow: hidden; height: 52%">
            <form action="" method="post" id="loginForm" class="w-75" align="center">
                <h2 class="text-center login-title">欢迎登录</h2>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="请输入职工号/学号">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="请输入密码">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" name="validateCode" class="form-control" placeholder="请输入验证码"></div>
                        <div class="col-md-4 text-right">
                            <a href="javascript:void(0);" title="点击更换验证码">
                                <img src="/captcha/captchaImage?t=math" class="imgCode" t="math" width="100%" height="35px"/>
                            </a>
                        </div>
                      </div>
                </div>

                <div class="form-group">
                    <input type="submit" id="btnSubmit" class="btn-login btn btn-primary btn-block" data-loading="正在验证登录，请稍后..." value="登录">
                </div>

       <div class="text-center">
           <a href="javascript:forgotPsw()"  style="color: white; font-size: 15px;">忘记密码？</a>

       </div>

            </form>

                <script type="text/javascript">

                    function forgotPsw(){
                        var html = "";
                        html += "<form name=\"emailForm\" class=\"form-horizontal\" role=\"form\" method=\"POST\" style=\"margin-top: 30px;\">";
                        html += "  <div class=\"form-group no-margin\">";
                        html += "    <label class=\"col-sm-3 control-label text-center forget-label\" for=\"email\">邮箱地址：</label>";
                        html += "    <div class=\"col-sm-6\">";
                        html += "      <input id=\"email\" type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"请输入邮箱地址\">";
                        html += "    </div>";
                        html += "    <div class=\"col-sm-3\">";
                        html += "       <button type=\"button\" class=\"btn btn-default\" onclick=\"submitEmail()\">发送验证码</button>";
                        html += "    </div> ";
                        html += "    </div> ";
                        html += "  <div class=\"form-group no-margin\">";
                        html += "    <label class=\"col-sm-3 control-label text-center forget-label\" for=\"captcha\">验证码：</label>";
                        html += "    <div class=\"col-sm-9\">";
                        html += "      <input id=\"captcha\" type=\"text\" class=\"form-control forget-input\" name=\"captcha\" placeholder=\"请输入验证码\" >";
                        html += "    </div>";
                        html += "  </div>";
                        html += "  <div class=\"form-group no-margin\">";
                        html += "    <label class=\"col-sm-3 control-label text-center forget-label\" for=\"forgetpassword\">新密码：</label>";
                        html += "    <div class=\"col-sm-9\">";
                        html += "      <input id=\"forget-password\" type=\"password\" class=\"form-control forget-input\" name=\"forget-password\" placeholder=\"请输入新密码\">";
                        html += "    </div>";
                        html += "  </div>";
                        html += "    <div class=\"form-group no-margin\">";
                        html += "      <label class=\"col-sm-3 control-label text-center forget-label\" for=\"re-forget-password\">再次输入密码：</label>";
                        html += "     <div class=\"col-sm-9\">";
                        html += "      <input id=\"re-forget-password\" type=\"password\" class=\"form-control forget-input\" name=\"re-forget-password\" placeholder=\"请再次输入密码\">";
                        html += "     </div>";
                        html += "     </div>";
                        html += "     <div class=\"col-sm-12 text-center \">";
                        html += "       <button type=\"button\" class=\"btn btn-primary\" style=\"width: 30%\" onclick=\"updatePasswordByCaptcha()\" id=\"submitpassword\">提交</button>";
                        html += "     </div> ";
                        html += "</form>";
                        html += " ";
                        html += " ";

                        layer.open({
                            type: 1,
                            title: '忘记密码',
                            shadeClose: true,
                            shade: 0.8,
                            area: ['50%', '40%'],
                            content:html
                        });
                    }


                    function submitEmail() {

                        if ($("#email").val() == "") {
                            layer.msg("邮箱不能为空!");
                            return;
                        }

                        $.ajax({
                            type: "POST",
                            url: "${ctx}/sendemail",
                            data : {
                                email : $("#email").val()
                            },
                            dataType: "json",
                            success: function (result) {
                                layer.msg(result.msg);
                            },
                            error: function (result) {
                                layer.msg(result.msg);
                            }

                        });
                    }


                    function updatePasswordByCaptcha() {

                        if ($("#email").val() == "") {
                            layer.msg("邮箱不能为空!");
                            return;
                        }

                        if ($("#captcha").val() == "") {
                            layer.msg("验证码不能为空!");
                            return;
                        }

                        if ($("#forget-password").val() == "" || $("#re-forget-password").val() == "") {
                            layer.msg("密码不能为空!");
                            return;
                        }

                        if ($("#forget-password").val() != $("#re-forget-password").val()) {
                            layer.msg("密码输入不一致");
                            return;
                        }

                        $.ajax({
                        type: "POST",
                            url: "${ctx}/updatePasswordByCaptcha",
                            data : {
                                email : $("#email").val(),
                                repassword : $("#forget-password").val(),
                                captcha : $("#captcha").val()

                            },
                            dataType: "json",
                            success: function (result) {
                                layer.msg(result.msg);
                            },
                            error: function (result) {
                                layer.msg(result.msg);
                            }
                        });
                    }
                </script>

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/plugins/jquery-validation/messages_zh.min.js"></script>
<script type="text/javascript" src="/js/plugins/blockUI/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="/js/plugins/layer/layer.js"></script>
<script type="text/javascript" src="/js/plugins/jsencrypt/jsencrypt.min.js"></script>
<script type="text/javascript" src="/js/login.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
</body>
</html>

